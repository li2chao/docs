# 1、RBAC授权访问控制

## 1.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context k8s
```

**Context**
为部署管道创建一个新的`ClusterRole`并将其绑定到指定范围为特定的namespace的特定`ServiceAccount`。

**Task**
创建一个名为`deployment-clusterrole`且仅允许创建以下资源类型的新`ClusterRole`：

- `Deployment`
- `StatefulSet`
- `DaemonSet`

在现有的namespace`app-team1`中创建一个名为`cicd-token`的新`ServiceAccount`。

限于namespace`app-team1`，将新的ClusterRole`deployment-clusterrole`绑定到新的ServiceAccount`cicd-token`。

## 1.2 考试答案

```Shell
1）设置配置环境
$ kubectl use-context k8s

2）解题
第一步：创建一个名为`deployment-clusterrole`且仅允许创建`Deployment、StatefulSet、DaemonSet`资源类型的新`ClusterRole`
$ kubectl creata clusterrole deployment-clusterrole --resource=deployment,statefulset,daemonset --verb=create

第二步：在现有的namespace`app-team1`中创建一个名为`cicd-token`的新`ServiceAccount`
$ kubectl create sa cicd-token -n app-team1

第三步：限于namespace`app-team1`，将新的ClusterRole`deployment-clusterrole`绑定到新的ServiceAccount`cicd-token`
$ kubectl create rolebinding cicd-token-rolebinding --serviceaccount=app-taem1:cicd-token --clusterrole=deployment-clusterrole -n app-team1
```

# 2、Node节点维护

## 2.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context ek8s
```

**Task**
将`ek8s-node-1`节点设置为不可用，然后重新调度该节点上的所有Pod。

## 2.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context ek8s

2）解题
第一步：将ek8s-node-1节点设置为不可用
$ kubectl cordon ek8s-node-1

第二步：重新调度该节点上的所有Pod（守护进程Pod除外）。
$ kubectl drain ek8s-node-1 --ignore-daemonsets --delete-emptydir-data --force
```

# 3、K8S集群版本升级

## 3.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context mk8s
```

**Task**
现有的Kubernetes集群正在运行版本1.23.1。**仅将master节点上**的所有Kubernetes控制平面和节点组件升级到版本1.23.2。

确保在升级之前`drain`主节点，并在升级后`uncordon`主节点。

```Shell
可使用以下命名通过`ssh`连接到主节点：
$ ssh -mk8s-matser-1

可使用以下命令在该主节点上获取更高权限：
$ sudo -i
```

另外，在主节点上升级`kubelet`和`kubectl`，请不要升级工作节点、etcd、container管理器、CNI插件、DNS服务或其他任何插件。

## 3.2 考试答案

> **官方文档**：[升级kubeadm集群](https://kubernetes.io/zh-cn/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/)
> **说明**：本考题**第三步**答案内容稍微多一些，不需要死记硬背，基本上是按照官方文档进行操作的，只需在考试前在官方文档中搜索\[kubeadm-upgrade\]关键字找到参考页，考试时按照官方文档一步步操作即可。
> **注意**：
> 
> - 1）将版本号更改为考题中要求的版本号；
> - 2）执行`kubeclt upgrade apply`命令时记得加上`--etcd-upgrade`选项，

```Shell
1）设置配置环境
$ kubectl config use-context mk8s

2）解题
第一步：确保在升级之前排空主节点
$ kubectl drain mk8s-master-1 --ignore-daemonsets --delete-emptydir-data --force

第二步：使用ssh连接到主节点，准备执行升级操作
$ ssh mk8s-master-1
$ sudo -i  -->  以目标用户运行一个登录shell

第三步：在主节点上升级kubelet和kubectl至1.23.2版本，请不要升级工作节点、etcd、container管理器、CNI插件、DNS服务或其他任何插件

[更新仓库、在仓库列表中查找1.23.2版本]
$ apt update
$ apt-cache show kubeadm | grep 1.23.2

[升级控制平台节点]
# 升级kubeadm
$ apt-mark unhold kubeadm && \
$ apt-get update && apt-get install -y kubeadm='1.23.2-00' && \
$ apt-mark hold kubeadm

# 验证下载操作正常，并且kubeadm版本正确
$ kubeadm version

# 验证升级计划
$ kubeadm upgrage plan

# 升级到1.23.2版本，忽略etcd升级（考题要求内容，必须加上此选项）
$ kubeadm upgrade apply v1.23.2 --etcd-upgrade=false

# 升级kubelet和kubectl
$ apt-mark unhold kubelet kubectl && \
$ apt-get update && apt-get install -y kubelet='1.23.2-00' kubectl='1.23.2-00' ** \
$ apt-mark hold kubelet kubectl

# 重启kubelet
$ systemctl daemon-reload
$ systemctl restart kubelet
$ kubelet --version

第四步：退出主节点shell终端，回到`kubectl`管理终端
$ exit  -->  退出`sudo -i`开启的shell终端
$ exit  -->  退出`ssh mk8s-matser-1`开启的shell终端

第四步：在升级后`uncordon`主节点
$ kubectl uncordon k8s-node-1
$ kubectl get node k8s-node-1
$ kubectl version
```

# 4、ETCD数据库备份恢复

## 4.1 考试内容

```Shell
此项目无需更改配置环境。但是，在执行此项目之前，请确保您已返回初始点：
[student@mk8s-master-1]$ exit  -->  注意，这个之前是在mk8s-master-1节点上，所以要`exit`退到node节点上，如果已经在node节点上了，就不要在`exit`了。
```

**Task**
首先，为运行在`https://127.0.0.1:2379`上的现有`etcd`实例创建快照并将快照保存到`/tmp/etcd-snapshot.db`。

> 为给定实例创建快照预计能在几秒钟内完成。如果该操作似乎挂起，则命令可能有问题。用`CTRL + C`来取消操作，然后重试。

然后还原位于`/tmp/etcd-snapshot-previous.db`的现有先前快照。

> 提供了以下TLS证书和密钥，以通过`etcdctl`连接到服务器。
> 
> - CA证书：/etc/kubernetes/pki/etcd/ca.crt
> - 客户端证书：/etc/kubernetes/pki/etcd/server.crt
> - 客户端密钥：/etc/kubernetes/pki/etcd/server.key

## 4.2 考试答案

> **官方文档**：[为Kubernetes运行etcd集群](https://kubernetes.io/zh-cn/docs/tasks/administer-cluster/configure-upgrade-etcd/)

```Shell
1）设置配置环境
在执行此项目之前，请确保您已返回初始点
[student@mk8s-master-1]$ exit  -->  注意，这个之前是在mk8s-master-1节点上，所以要`exit`退到node节点上，如果已经在node节点上了，就不要在`exit`了

2）解题
第一步：为运行在`https://127.0.0.1:2379`上的现有`etcd`实例创建快照并将快照保存到`/data/backup/etcd-snapshot.db`
ETCDCTL_API=3 etcdctl --endpoints=https://127.0.0.1:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key snapshot save /tmp/etcd-snapshot.db

第二步：还原位于`/tmp/etcd-snapshot-previous.db`的现有先前快照。
$ ETCDCTL=3 etcdctl --endpoints=https://127.0.0.1:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key snapshot restore /tmp/etcd-snapshot-previous.db
```

# 5、Network Policy网络策略

## 5.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context hk8s
```

**Task**
基于已存在的`topic-5`名称空间中创建一个名为`allow-port`的新NetworkPolicy。

确保新NetworkPolicy允许`echo`名称空间中的Pods连接到名称空间`topic-5`中的Pods的`9900`端口。

进一步确保新的NetworkPolicy：

- 不允许对没有在监听端口9900的Pods的访问
- 不允许非来自名称空间`echo`中的Pods的访问

## 5.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context hk8s

2）解题
第一步：查看`topic-5`名字空间是否存在。
- 如果存在，执行第三步
- 如果不存在，创建`topic-5`名字空间，再执行第三步
$ kubectl get ns | grep topic-5

第二步：基于已存在的`topic-5`名称空间中创建一个名为`allow-port`的新NetworkPolicy
[查看`echo`名字空间拥有的标签]
$ kubectl get ns --show-labels | grep echo
- 如果存在标准标签，直接使用
- 如果不存在任何标签，设置标准标签
$ kubectl label ns echo kubernetes.io/metadata.name=echo

[编写NetworkPolicy资源清单]
$ vim allow-port.yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-port
  namespace: topic-5
spec:
  podSelector: {}  # 允许`echo`名称空间中的Pods连接到名称空间`topic-5`中的Pods的`9900`端口。
  policyTypes:
  - Ingress
  ingress:
  - ports:
    - port: 9900  # 允许`echo`名称空间中的Pods连接到名称空间`topic-5`中的Pods的`9900`端口。
    from:
    - namespaceSelector:
        matchLabels:
          kubernetes.io/metadata.name: echo  # 允许`echo`名称空间中的Pods连接到名称空间`topic-5`中的Pods的`9900`端口。

[应用NetwokPolicy，使其生效]
$ kubectl apply -f allow-port.yaml
```

# 6、Service NodePort

## 6.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context k8s
```

**Task**
请重新配置现有的部署`front-end`以及添加名为`http`的端口规范来公开现有容器`nginx`的端口`80/tcp`。

创建一个名为`front-end-svc`的新服务，以公开容器端口`http`。

配置此服务，以通过在排定的节点上的**NodePort**来公开各个Pods。

## 6.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context k8s

2）解题
第一步：重新配置现有的部署`front-end`以及添加名为`http`的端口规范来公开现有容器`nginx`的端口`80/tcp`。
$ kubectl edit deploy front-end
... ...
containers:
  ports:
  - name: http
    protocol: tcp
    containerPort: 80
... ...

第二步：
- 创建一个名为`front-end-svc`的新服务，以公开容器端口`http`。
- 配置此服务，以通过在排定的节点上的NodePort来公开各个Pods。
$ kubectl expose deploy front-end --type=NodePort --port=80 --target-port=http --name=front-end-svc

第三步：验证结果
$ kubectl describe svc front-end-svc
... ...
Endpoints: 目标Pods[IP:80]列表
... ...
```

# 7、Ingress

## 7.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context k8s
```

**Task**
如下创建一个新的nginx Ingress资源：

- 名称：`pong`
- Namespace：`ing-internal`
- 使用服务端口`5678`在路径`/hello`上公开服务`hello`

```Shell
可以使用以下命令检查服务`go`的可用性，该命令返回`hello`：
$ curl -kL <INTERNAL_IP>/hello
```

## 7.2 考试答案

> **官方文档（考试时需要参考此标签页）**：[Ingress](https://kubernetes.io/zh-cn/docs/concepts/services-networking/ingress/)

```Shell
1）设置配置环境
$ kubectl config use-context k8s

2）解题
第一步：查看是否存在可用的IngressClass
- 如果不存在，放弃此题
- 如果存在，记住IngressClassName，进行第二步
$ kubectl get ingressclass

第二步：创建一个新的nginx Ingress资源，名称为`pong`、名字空间为`ing-internal`、使用服务端口`5678`在路径`/hello`上公开服务`hello`
$ kubectl create ing pong --class=IngressClassName --rule="/hello=hello:5678" -n ing-internal

第三步：获取Ingress IP地址，验证结果
$ kubectl get ing pong -n ing-internal -o wide
$ curl -kL internal_ip/hello
```

# 8、扩容Deployment副本数量

## 8.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context k8s
```

**Task**
将deployment从`loadbalancer`扩展至`5`pods。

## 8.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context k8s

2）解题
将deployment从`loadbalancer`扩展至`5`pods
$ kubectl scale deploy loadbalancer --replicas=5
```

# 9、调度pod到指定节点

## 9.1 考试内容

```Shell
设置配置环境
$ kubectl config use-context k8s
```

**Task**
按如下要求调度一个pod：

- 名称：`nginx-kusc00401`
- Image：`nginx`
- Node Selector：`disk=ssd`

## 9.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context k8s

2）解题
按要求将名称为`nginx-kusc00401`，镜像为`nginx`的pod调度到标签为`disk=ssd`的节点上
$ vim pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-kusc00401
spec:
  nodeSelector:
    disk: ssd  
  containers:
  - name: nginx-kusc00401
    image: nginx
    
$ kubectl apply -f pod.yaml

3）检查结果
$ kubectl get pod nginx-kusc00401 -o wide
```

# 10、检查可调度的Node节点数量

## 10.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context k8s
```

**Task**
检查集群中有多少个节点为Ready状态（不包括被打上`Taint：NoSchedule`的节点），之后将数量写入`/tmp/kube-node.txt`文件中。

## 10.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context k8s

2）解题
第一步：检查集群中有多少个节点为Ready状态，获取节点名称清单
$ READY_NODES=$(kubectl get nodes | grep -w Ready | awk '{print $1}')

第二步：不包括被打上`Taint：NoSchedule`的节点，之后将数量写入`/tmp/kube-node.txt`文件中。
$ kubectl describe nodes $READY_NODES | grep Taints | grep -vc NoSchedule > /tmp/kube-node.txt

3）检查结果
$ cat /tmp/kube-node.txt
```

# 11、创建多容器的Pod

## 11.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context k8s
```

**Task**
创建一个名为`kucc4`的pod，在pod里面分别为以下每个images单独运行一个app container（可能会有1-4个images）：`nginx + redis + memcached + consul`。

## 11.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context k8s

2）解题
按要求创建一个包含多个容器的Pod
$ vim kucc4.yaml
apiVersion: v1
kind: Pod
metadata:
  name: kucc4
spec:
  containers:
  - name: nginx
    image: nginx
  - name: redis
    image: redis
  - name: memcached
    image: memcached
  - name: consul
    image: consul
$ kubectl apply -f kucc4.yaml

3）验证结果
$ kubectl get pod kucc4
```

# 12、创建PV

## 12.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context hk8s
```

**Task**
创建名为`app-data`的PersistentVolume，容量为`2Gi`，访问模式为`ReadWriteOnce`，volume类型为`hostPath`，位于`/srv/app-data`。

## 12.2 考试答案

> **官方文档（考试时需要参考此标签页）**：[配置Pod以使用PersistentVolume作为存储](https://kubernetes.io/zh-cn/docs/tasks/configure-pod-container/configure-persistent-volume-storage/)

```Shell
1）设置配置环境
$ kubectl config -use-context hk8s

2）解题
创建一个满足要求的PV卷
$ vim app-data.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: app-data
spec:
  capacity:
    storage: 2Gi
  accessModes:
  - ReadWriteOnce
  hostPath:
  - type: DirectoryOrCreate
    path: /srv/app-data
$ kubectl apply -f app-data.yaml

3）检查结果
$ kubectl get pv app-data
```

# 13、创建PVC

## 13.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context ok8s
```

**Task**
创建一个新的`PersistentVolumeClaim`：

- 名称：`pv-volume`
- Class：`csi-hostpath-sc`
- 容量：10Mi

创建一个新的Pod，此Pod将作为volume挂载到`PersistentVolumeClaim`：

- 名称：`web-server`
- Image：`nginx`
- 挂载路径：`/usr/share/nginx/html`

配置新的Pod，以对volume具有ReadWriteOnce权限。

最后，使用`kubectl edit`或`kubectl patch`将`PersistentVolumeClaim`的容量扩展为`70Mi`，并记录此更改。

## 13.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context ok8s

2）解题
第一步：按要求创建一个PVC
$ vim pv-volume.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pv-volume
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 10Mi
  storageClassName: csi-hostpath-sc
$ kubectl apply -f pv-volume.yaml

第二步：按要求创建一个Pod，并使用第一步创建的PVC作为数据持久卷
$ vim web-server.yaml
apiVersion: v1
kind: Pod
metadata:
  name: web-server
spec:
  containers:
  - name: web-server
    image: nginx
    volumeMounts:
    - name: data
      mountPath: /usr/share/nginx/html
  volumes:
  - name: data
    persistentVolumeClaim:
      claimName: pv-volume
$ kubectl apply -f web-server.yaml

第三步：按要求将`PersistentVolumeClaim`的容量扩展为`70Mi`，并记录此更改。
$ kubectl edit pvc pv-volume --save-config
... ...
resources:
  requests:
    storage: 10Mi --> 70Mi
... ...
```

# 14、获取Pod错误日志

## 14.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context k8s
```

**Task**
监控pod bar的日志并：

- 提取与错误`file-not-found`相对应的日志行
- 将这些日志行写入`/opt/KUTR00101/bar`

## 14.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-contxt k8s

2）解题
按要求提取错误日志并写入到指定文件内
$ kubectl logs bar | grep file-not-fount > /opt/KUTR00101/bar

3）检查结果
$ cat /opt/KUTR00101/bar
```

# 15、使用Sidecar（边车）代理容器日志

## 15.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context k8s
```

**Context**
将一个现有的Pod集成到Kubernetes的内置日志记录体系结构中（例如kubectl logs）。添加`streaming sidecar`容器是实现此要求的一种好方法。

**Task**
使用`busybox`Image来将名为`sidecar`的Sidecar容器添加到现有的Pod `legacy-app`中。新的Sidecar容器必须运行以下命令：

```Shell
/bin/sh -c tail -n+1 -f /var/log/legacy-app.log
```

使用安装在`/var/log`的Volume，使日志文件`legecy-app.log`可用于Sidecar容器。

> 除了添加所需的volume mount以外，请勿更改现有容器的规格。

## 15.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context k8s

2）解题
第一步：获取名为`legecy-app`的Pod的资源清单
$ kubectl get pod legecy-app -o yaml > sidecar.yaml

第二步：删除名为`legecy-app`的Pod，并修改资源清单，增加Sidecar容器和日志共享卷
$ kubectl delete pod legecty-app
$ vim sidecar.yaml
... ...
spec:
  volumes:
  - name: logs
    emptyDir: {}
  containers:
  - name: xx
    image: xx
    volumeMounts:
    - name: logs
      mountPath: /var/log
  - name: sidecar
    image: busybox
    args: ["/bin/sh" "-c" "tail -n+1 -f /var/log/legacy-app.log"]
    volumeMounts:
    - name: logs
      mountPath: /var/log

第三步：按要求创建一个新的Pod
$ kubectl apply -f sidecar.yaml

3）检查结果
$ kubectl logs legecy-app -c sidecar -f 
```

# 16、统计使用CPU最高的Pod

## 16.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context k8s
```

**Task**
通过pod label `name=cpu-utilizer`，找到运行时占用大量CPU的Pod，并将占用CPU最高的Pod名称写入文件`/opt/KUTR00401/KUTR00401.txt`（已存在）。

## 16.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context k8s

2）解题
按要求找到运行时占用大量CPU的Pod，并将占用CPU最高的Pod名称写入指定的文件中
$ kubectl top pod -l name=cpu-utilizer --sort-by=cpu
$ echo "max-cpu-utilizer-pod-name" > /opt/KUTR00401/KUTR00401.txt
```

# 17、排查集群中的故障节点

## 17.1 考试内容

```Shell
设置配置环境：
$ kubectl config use-context wk8s
```

**Task**
名为`wk8s-node-0`的Kubernetes worker node处于`NotReady`状态。调查发现这种情况的原因，并采用相应措施将node恢复为Ready状态，确保所做的任何更改永久有效。

```Shell
可使用以下命令通过`ssh`连接到故障node：
$ ssh wk8s-node-0

可使用以下命令在该node上获得更高权限：
$ sudo -i
```

## 17.2 考试答案

```Shell
1）设置配置环境
$ kubectl config use-context wk8s

2）解题
第一步：进入故障节点
$ ssh wk8s-node-0
$ sudo -i

第二步：重启kubelet，并将其加入开启自启动
$ systemctl status kubelet  -->  真实考试中，异常原因为：故障节点的kubelet服务没有启动，就这么简单，只需将kubelet服务重启一下，并加入系统自启动即可。
$ systemctl restart kubelet
$ systemctl enbale kubelet

第三步：退出至初始终端
$ exit
$ exit

3）检查结果
$ kubectl get nodes
```