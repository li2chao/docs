> 说明：
> [官方文档](https://git-scm.com/book/zh/v2) 已经给出非全全面的讲解，本处只记录学习感悟和总结，以帮助我们快速掌握这个知识点。
# 1、git config

```
$ git config --help | -h
## NAME
获取并设置或系统、或全局、或存储库的配置内容的选项。

## DESCRIPTION
1、读取时，默认情况下会从系统、全局和存储库本地配置文件中读取值，选项--system、--global、--local 可用于指示命令仅从该位置读取。
	# 对应配置文件路径如下：
	--system  /etc/gitconfig  # 包含系统上每一个用户及他们仓库的通用配置。
	--global  $HOME/.gitconfig  # 针对当前用户的全局配置。
	--local   $REPOSITORY/.git/config  # 针对该仓库的本地配置。
2、写入时，默认情况下，新值会写入存储库本地配置文件，选项 --local、--global、--system 可用于通知命令写入该位置，默认值为：--local。
可以使用 [git help --config] 命令获得所有可用配置变量的列表。
> 常用配置变量及其值：
[core]
	## 配置 Git 使用的文本编辑器
	editor = "'C:\Program Files (x86)\Notepad++\notepad++.exe' -multiInst -notabbar -nosession -noPlugin"
[alias]
	## 配置 Git 命令中使用的别名
	co = checkout
[user]
	## 设置用户名和邮件地址，这一点很重要，因为每一个 Git 提交都会使用这些信息，它们会写入到你的每一次提交中，不可更改。
	name = LiChao
	email = 18810396626@163.com
[remote "origin"]  # 定义了一个名为 origin 的（默认）远程仓库
	url = https://gitlab.com/li2chao/docs.git  # 远程仓库 URL
	fetch = +refs/heads/*:refs/remotes/origin/**  # 如何获取远程分支，内容表示从远程仓库获取所有以 refs/heads/* 开头的引用（即所有分支），并将它们映射到本地的 refs/remotes/origin/** 的相应位置。
[branch "main"]  # 定义了一个名为 mian 的本地分支。
	## 配置本地 main 分支信息
	remote = origin  # 设置本地 main 分支的远程仓库为 origin，这意味着本地 main 分支的所有更改都将推送到 origin 远程仓库。
	merge = refs/heads/main  # 设置本地 main 分支的合并引用，这意味着当本地 main 分支需要合并时，Git 将使用 refs/heads/main（远程仓库中 mian 分支的最新提交）作为合并的基础。
待补充 ... ...

## OPTIONS
> 常用选项及说明：
$ git config
		--system | --global | --local  # 从系统、全局或存储库中获取及设置配置内容，默认值为：--local。
		-l | --list  # 列出配置文件中设置的所有变量及其值。
		--show-origin  # 使用原始类型（文件、标准输入、blob、命令行）和实际原始（配置文件路径、ref或blob id）增强所有查询的配置选项的输出。
		-e | --edit  # 打开编辑器以修改指定的配置文件；系统、全局或存储库（默认）。
		--unset | --unset-all # 从配置文件中删除（所有）与键匹配的行。

## EXAMPLES
1）配置 Git 使用的文本编辑器
$ git config --global core.editor "'C:\Program Files (x86)\Notepad++\notepad++.exe' -multiInst -notabbar -nosession -noPlugin"

2）设置用户名和邮件地址
$ git config --global user.name "LiChao"
$ git config --global user.email "18810396626@163.com"

3）列出配置文件中设置的所有变量及其值
$ git config -l --show-origin
file:C:/Program Files/Git/etc/gitconfig	diff.astextplain.textconv=astextplain
... ...
file:C:/Users/zawm/.gitconfig	user.name=LiChao
file:C:/Users/zawm/.gitconfig	user.email=18810396626@163.com
... ...
file:.git/config	core.repositoryformatversion=0
file:.git/config	core.filemode=false
... ...

4）从配置文件中删除（所有）与键匹配的行
$ git config --global --unset core.editor
```

# 2、git init

```
$ git init --help | -h
## NAME
创建一个空的 Git 存储库或重新初始化现有的存储库。

## DESCRIPTION
当在一个新目录或已有目录执行 git init 时，Git 会创建一个 .git 目录，并创建一个名称为 master | main 的起始分支名称。 这个目录包含了几乎所有 Git 存储和操作的东西。 如若想备份或复制一个版本库，只需把这个目录拷贝至另一处即可。

此外，在现有存储库中运行 git init 是安全的。它不会覆盖已经存在的内容。重新运行 git init 的主要原因是获取新添加的模板（或者如果给定了单独的 GIT_DIR，则将存储库移动到另一个位置）。

新初始化的 .git 目录的典型结构如下（只关注核心内容，省略了不重要的文件或目录）：
|-- ./.git  # 存放存储库中的所有数据及信息。如果设置了 $GIT_DIR 环境变量，则它会指定要使用的路径，而不是 ./ 。
	|-- config  # 配置文件，存储库特有的配置选项。
	|-- HEAD  [ref: refs/heads/main]  # 符号引用，当前分支的别名，内容为指向当前分支的引用路径。
	|-- index  # 索引文件，存放暂存区的信息。
	|-- objects/  # 对象数据库，存放存储库中的所有已经提交的数据。如果对象存储目录是通过 $GIT_OBJECT_DIRECTORY 环境变量指定的，则会在下面创建 sha1 目录，否则将使用默认的 $GIT_DIR/objects 目录。
	|-- info/  # 忽略信息，包含一个全局性排除（global exclude）文件， 用以放置那些不希望被记录在 .gitignore 文件中的忽略模式（ignored patterns）。
		|-- exclude  # 存放被忽略文件的 glob 规则信息。
	|-- refs/  #  引用信息，存放所有的引用信息。所谓引用，本质上就是数据对象、树对象、提交对象的 SHA-1 值的别名。
		|-- heads/  # 本地分支信息。所谓分支，本质上就是一系列提交之首的 SHA-1 值的别名。
			|-- main  [5448049ea9af0d2aa33667444fd9c67b9e67cde6]  # 本地 main 分支
		|-- remotes/  # 远程跟踪分支（只读）信息，所谓远程跟踪分支，指的是远程分支状态的引用，它们是你无法移动的本地引用。一旦你进行了网络通信， Git 就会为你移动它们以精确反映远程仓库的状态。请将它们看做书签， 这样可以提醒你该分支在远程仓库中的位置就是你最后一次连接到它们的位置。
			|-- origin/  # 远程仓库的名称
				|-- main  [5448049ea9af0d2aa33667444fd9c67b9e67cde6]  # 远程仓库 main 分支
				|-- HEAD  [ref: refs/remotes/origin/main]  # HEAD 符号引用，内容为指向远程仓库当前分支的引用路径
		|-- tags/  # 所有标签信息。所谓标签，本质上就是一个引用，分为轻量标签和附注标签。所谓轻量标签，本质上就是一个固定的引用；所谓附注标签，是一个指向标签对象的引用路径，标签对象的实现可以参考提交对象，提交对象本质是记录顶层树对象和父提交对象的 SHA-1 值的一种特殊的对象，另外包含了 who（提交者）、when（提交实践）、what（提交说明） 等信息；而标签对象本质是记录 Git 中各种对象的 SHA-1 值的一种特殊的对象，另外包含了 who（提交者）、when（提交实践）、what（提交说明） 等信息。
	|-- hooks/  # 钩子信息，包含客户端或服务端的钩子脚本（hook scripts）。
	|-- logs/  # 日志信息，存放所有的日志信息。
	|-- ... ...

## OPTIONS
$ git init
		--bare  # 创建一个裸存储库。如果未设置 GIT_DIR 环境，则将其设置为当前工作目录。
		--shared  # 指定 Git 存储库要在多个用户之间共享，即修改该仓库目录的组权限为可写，其本质是修改服务器操作系统的文件系统权限。这允许属于同一组的用户推入该存储库。指定时，会设置配置变量 core.sharedRepository，以便使用请求的权限创建 $GIT_DIR 下的文件和目录。

## EXAMPLES
1）使用 SSH 连接到一个服务器，初始化一个裸仓库，并修改该仓库目录的组权限，允许属于同一组的用户具有写权限
$ ssh user@git.example.com
$ cd /srv/git/my_project.git
$ git init --bare --shared
```

# 3、远程仓库

## 3.1 git remote

```
$ git remote --help | -h
## NAME
管理一组跟踪的远程仓库服务器。

## DESCRIPTION
远程仓库是指托管在因特网或其他网络中的你的项目的版本库。 
你可以有好几个远程仓库，通常有些仓库对你只读，有些则可以读写。 
管理远程仓库包括了解如何添加远程仓库、移除无效的远程仓库、管理不同的远程分支并定义它们是否被跟踪等等。
与他人协作涉及管理远程仓库以及根据需要推送或拉取数据。 

## OPTIONS
$ git remote
		-v | --verbose  # 显示需要读写远程仓库使用的 Git 保存的简写与其对应的 URL。

## SUB-COMMANDS
$ git remote
	show <name>  # 查看某一个远程仓库的详细信息。
	add <name> <url>  # 添加一个新的远程 Git 仓库，同时指定一个方便使用的简写。
	rename <old> <new>  # 修改一个远程仓库的简写名。需要注意的是这同样也会修改你所有远程跟踪的分支名字。
	rm | remove <name>  # 移除一个远程仓库，一旦你使用这种方式删除了一个远程仓库，那么所有和这个远程仓库相关的远程跟踪分支以及配置信息也会一起被删除。
	prune <name> # 删除与 <name> 关联的过时引用。等效于git fetch <name> --prune。

## EXAMPLES
1）显示 Git 保存的远程仓库的简写与其对应的 URL
$ git remote -v  # 说明：如果你使用 git clone 命令克隆了一个仓库，命令会自动将其添加为远程仓库并默认以 origin 为简写，如下所示。
origin  https://gitlab.com/li2chao/docs.git(fetch)  # 用于获取远程仓库数据的 URL
origin  https://gitlab.com/li2chao/docs.git(push)  # 用于推送本地仓库数据到远程仓库的 URL
... ...

2）查看某一个远程仓库的详细信息
$ git remote show origin
* remote origin  # [*] 表示当前默认的远程仓库地址为 origin
  URL: https://github.com/my-org/complex-project  # 远程仓库的 URL
  Fetch URL: https://github.com/my-org/complex-project  # 用于获取远程仓库数据的 URL
  Push  URL: https://github.com/my-org/complex-project  # 用于推送本地仓库数据到远程仓库的 URL
  HEAD branch: master  # 当前 HEAD 指向的分支名，当前正处于 master 分支
  Remote branches:  # 远程仓库的所有分支
    master                           tracked  # 这是一个跟踪分支，本地和远程都有这个分支
    dev-branch                       tracked  # 同上
    markdown-strip                   tracked  # 同上
    issue-43                         new (next fetch will store in remotes/origin)  # 这是一个新创建的分支，将再下一次 fetch 操作时存储在 remotes/origin 分支中。
    issue-45                         new (next fetch will store in remotes/origin)  # 同上
    refs/remotes/origin/issue-11     stale (use 'git remote prune' to remove)  # 这是一个过期的引用，可以使用 git remote prune 命令来移除。
  Local branches configured for 'git pull':  # 所有配置为 git pull 操作的本地分支
    dev-branch merges with remote dev-branch  # 本地的 dev-branch 分支会与远程的 dev-branch 分支合并
    master     merges with remote master  # 同上
  Local refs configured for 'git push':  # 所有配置为 git push 操作的本地引用
    dev-branch                     pushes to dev-branch                     (up to date)  # 本地的 dev-branch 分支会推送到远程的 dev-branch 分支，且状态为最新
    markdown-strip                 pushes to markdown-strip                 (up to date)  # 同上
    master                         pushes to master                         (fast-forwardable)  # 本地的 master 分支会被推送到远程的 master 分支，并且可以进行快速推送，这意味着在本地和远程的 matser 分支之间没有需要合并的冲突或更改。
```

### 3.1.1 git ls-remote（列出远程引用列表）

```
$ git ls-remote <remote>  # 获取远程引用（HEAD 引用、分支、标签）的完整列表
24ce18b21b56b00fc18410d7cd9d026b5eb80f75        HEAD
24ce18b21b56b00fc18410d7cd9d026b5eb80f75        refs/heads/main
```

## 3.2 git clone

```
$ git clone --help | -h
## NAME
将存储库克隆到新目录中。

## DESCRIPTION
下载整个项目的历史，包括每一个文件的每一个版本，将其克隆到新目录中，并在这个目录下初始化一个 .git 文件夹， 从远程仓库拉取下所有数据放入 .git 文件夹，然后为克隆的存储库中的每个分支创建远程跟踪分支（使用 git branch --remotes 可见），并创建、签出从克隆的存储池的当前活动分支分叉的初始分支。

即 Git 克隆的是该 Git 仓库服务器上的几乎所有数据，而不是仅仅复制完成你的工作所需要文件。当你执行 git clone 命令的时候，默认配置下远程 Git 仓库中的每一个文件的每一个版本都将被拉取下来。

例如，如果你 git.ourcompany.com 克隆，Git 的 clone 命令会为你自动将其命名为 origin（默认远程仓库名称），拉取它的所有数据，创建一个指向它的 master 分支的指针，并且在本地将其命名为 origin/master。Git 也会给你一个与 origin 的 master 分支在指向同一个地方的本地 master 分支，这样你就有了工作的基础。

## OPTIONS
$ git clone 
		--bare  # 制作一个裸露的 Git 存储库，即只取出 Git 仓库自身，不要工作目录，然后特别为它单独创建一个目录。也就是说，与其创建工作目录并将管理文件放在工作目录/.git 中，不如将目录本身设置为 $GIT_DIR。这显然意味着 --no-checkout，因为没有地方可以签出工作树。此外，远程的分支头被直接复制到相应的本地分支头，而不将它们映射到 refs/remotes/origin/。使用此选项时，既不会创建远程跟踪分支，也不会创建相关的配置变量。

## EXAMPLES
1）克隆远程仓库
$ git clone https://github.com/libgit2/libgit2  # 这会在当前目录下创建一个名为 libgit2 的目录，并在这个目录下初始化一个 .git 文件夹， 从远程仓库拉取下所有数据放入 .git 文件夹，然后从中读取最新版本的文件的拷贝。

2）克隆远程仓库，自定义本地仓库的名字
$ git clone https://github.com/libgit2/libgit2 mylibgit  # 这会执行与上一条命令相同的操作，但目标目录名变为了 `mylibgit`。

3）克隆本地仓库，创建一个新的裸仓库（不带工作目录的 Git 仓库）
$ git clone --bare my_project my_project.git
==
$ cp -Rf my_project/.git my_project.git
```

## 3.3 git 分支

### 3.3.1 git fetch

```
$ git fetch --help
## NAME
从远程仓库获取（所有）分支的提交更新。

## DESCRIPTION
从一个或多个其他存储库中获取分支和 / 或标记（统称为 “refs”），以及完成其历史记录所需的对象。远程跟踪分支将被更新。

使用 git fetch 从远程仓库获取你所需的所有分支的最新提交，但不会自动合并或修改你当前（分支）的工作目录。这就意味着，你可以查看、比较甚至合并远程分支的最新提交，而无需将这些更改应用到你的工作目录。

如果你的当前分支设置了跟踪远程分支， 可以用 git pull = git fetch + git merge 命令来自动拉取后合并该远程分支到当前分支。默认情况下，git clone 命令会自动设置本地 master 分支跟踪克隆的远程仓库的 master 分支（或其它名字的默认分支）。 运行 git pull 通常会从最初克隆的服务器上拉取数据并自动尝试合并到当前所在的分支。

## OPTIONS
$ git fetch --all | <remote> | <repository> 
		<branch>  # 从远程仓库获取指定分支的最新提交，默认为所有分支。
		<refspec>  # 指定要获取的远程分支和要更新的本地分支。当命令行中没有 <refspec> 时，将从 git config 配置文件 [remote "repository"] 读取要获取的 fetch 变量（fetch = +refs/heads/*:refs/remotes/origin/**，将所有远程分支拉取到本地）。
		-p | --prune  # 在 fetch 之前，请删除远程上不再存在的任何本地引用。
		-n | --no-tags  # 默认情况下，远程仓库对象的标记将被提取并存储在本地。此选项禁用此自动标记跟随。
		-v | --verbose  # 输出详细信息。

## EXAMPLES
1）获取远程 origin 仓库的所有分支的更新
$ git fetch origin

2）获取远程 origin 仓库的 master 分支的更新
$ git fetch origin matser
```

### 3.3.2 git branch

```
$  git branch --help | -h
## NAME
列出、创建或删除分支。

## DESCRIPTION
所谓分支，指的是一系列提交之首（最新提交）对象的命名简称，记录了一系列的提交历史（通过父提交进行追溯）。因此，分支在 Git 中用于隔离不同功能或任务的代码区域，使得开发者可以定义自己的分支进行独立地开发和测试，而不影响其他分支（主分支）上的代码。

## OPTIONS
$ git branch 
		[<名称>] [<commit> | <branch>]  # 不指定名称，默认为列出所有分支，[*] 指明当前分支；指定一个不存在的分支名称，实现创建分支；新分支默认指向当前提交对象，如果分支名称后面附加一个 <commit> | <branch>，指向具体的提交对象或分支。
		-v | --verbose  # 查看每一个分支的最后一次提交。
		-vv  # 将所有的本地分支列出来并且包含更多的信息，如查看每一个分支的最后一次提交，每一个分支正在跟踪哪个远程分支与本地分支是否是领先、落后等。 这个命令并没有连接服务器，它只会告诉你关于本地缓存的远程分支数据。如果想要统计最新的领先与落后数字，需要在运行此命令前使用 git fetch --all 抓取所有的远程仓库信息。
		--merge  # 查看哪些分支已经合并到当前分支或指定分支，因为你已经将它们的工作整合到了另一个分支，所以并不会失去任何东西，通常可以使用 git branch -d 删除掉。
		--no-merge  # 查看哪些分支还未合并到当前分支或指定分支，因为它包含了还未合并的工作，尝试使用 git branch -d 命令删除它时会失败，如果真的想要删除分支并丢掉那些工作，可以使用 -D 选项强制删除它。
		-d | --delete  # 删除一个分支
		-D | --force  # 强制删除一个分支，即使它有未合并的提交。
		-m | --move  # 重命名一个分支
			$ git branch -m src dst  # src 为源分支名称，dst 为一个不存在的目标分支名称，将 src 分支重命名为 dst 分支。
		-M | --force-move  # 强制重命名一个分支，即使有未合并的提交。
		-u | --set-upstream-to  # 修改当前分支正在跟踪的上游分支。

## EXAMPLES
1）列出所有分支，并显示详细信息
$ git branch -v
  dev  b2ff05a new file [pretty.log]  # dev 分支，最新提交为 b2ff05a，没有跟踪任何远程分支，有一个新文件被添加到了该提交中，文件名为 pretty.log
* main b2ff05a [ahead 6, behind 1] new file [pretty.log]  # 当前分支为 main 分支，最新提交为 b2ff05a，本地有 6 个提交还没有推送到远程仓库，远程仓库有 1 个提交还没有合并到本地分支，有一个新文件被添加到了该提交中，文件名为 pretty.log

2）创建一个分支
$ git branch test

3）创建一个分支，并指向某个提交对象或分支
$ git branch test ab1afef | master

4）删除一个分支
$ git branch -d test

5）重命名一个分支
$ git branch -m main master

6）查看哪些分支还尚未合并到 master 分支
$ git branch --no-merged master
  topicA
  featureB

7）修改当前分支正在跟踪的远程分支
$ git branch -u origin/main
branch 'main' set up to track 'origin/main'.  # 分支 main 设置为跟踪 origin/main。
```

#### 3.3.2.1 git rev-list（列出分支提交历史）

```
$ git rev-list --help | -h
## NAME
rev-list == reverse list，按时间倒序列出提交对象。

## DESCRIPTION
列出可通过遵循给定提交的父链接访问的提交，但排除可从前面带有 ^ 的提交访问的提交。默认情况下，输出按逆时间顺序给出。
你可以把它看作一个集合运算。从命令行上给定的任何提交都可以到达的提交形成一个集合，然后从该集合中减去从前面给定的任何带有 ^ 的提交都可以达到的提交。剩下的提交是命令输出中的内容。可以使用各种其他选项和路径参数来进一步限制结果。
例如：
1. $ git rev-list main test ^dev  # 列出所有可以从 main 或 test 访问，但不能从 dev 访问的提交。
2. $ git rev-list A...B == $ git rev-list B ^A  == $ git rev-list A B --not $(git merge-base --all A B) # 列出从 A 分支到 B 分支的提交。
```

#### 3.3.2.2 git rev-parse（列出分支提交历史之首）

```
$ git rev-parse --help | -h
## NAME
rev-parse == reverse parse，列出分支最新的提交对象。
```

### 3.3.3 git checkout（old）

```
$ git checkout --help | -h
## NAME
切换分支或恢复工作区到指定提交。

## DESCRIPTION
它可以用于创建新分支，切换分支、重置工作区到指定提交等操作。

## OPTIONS
$ git checkout
		-b | --branch  # 创建并切换到一个目标分支，如果目标分支已存在，则操作失败。
		-f | --force  # 强制切换到目标分支，不进行任何提示。
		-t | --track  # 检出远程分支，显式地创建一个本地分支，新分支的名称将从远程跟踪分支派生，方法是查看远程跟踪分支部分。这将告诉我们在从 origin/ack（或 remotes/origin/acking，甚至 refs/remotes/original/hack）分支时使用 hack 作为本地分支。
		<目标分支>

## EXAMPLES
1）创建并切换到 test 分支
$ git checkout -b test

2）切换到远程 dev 分支
$ git checkout origin/dev

3）切换到指定提交（慎用）
$ git checkout abcd2f
```

### 3.3.4 git switch

```
$ git switch --help | -h
## NAME
切换分支。

## DESCRIPTION
它可以用于创建新分支，切换分支等操作。
所谓切换分支，指的是将工作目录切换到目标分支最后一次提交时的内容，并将 HEAD 引用路径指向目标分支。从效果上来说，就是忽略原分支所做的修改，以便于向另一个方向进行开发。

注意，在切换分支前，要留意你的工作目录和暂存区里那些还没有被提交的修改，因为当你切换分支的时候，Git 会重置你的工作目录，使其看起来像回到了你在那个分支上最后一次提交的样子，如果你的工作目录和暂存区里存在修改记录，它可能会和你即将检出的分支产生冲突从而阻止 Git 切换到该分支。最好的方法是，在你切换分支之前，保持好一个干净的状态。

## OPTIONS
$ git switch <branch> [<remote_branch>]
		-c | --create  # # 创建并切换到一个目标分支，如果目标分支已存在，则操作失败。
		-f | --force  # 强制切换到目标分支，不进行任何提示。
		-t | --track  # 检出远程分支，显式地创建一个本地分支，新分支的名称将从远程跟踪分支派生，方法是查看远程跟踪分支部分。这将告诉我们在从 origin/ack（或 remotes/origin/acking，甚至 refs/remotes/original/hack）分支时使用 hack 作为本地分支。
	

## EXAMPLES
1）创建并切换到 test 分支
$ git switch -c test

2）基于远程 origin/dev 分支创建并切换到本地 dev-a 分支
$ git switch -c dev-a origin/dev
```

### 3.3.3-4 git checkout 和 git switch 的区别

`git checkout` 和 `git switch` 都是 Git 中用于切换分支的命令，但他们存在一些本质区别。

- 1. 从版本演变上来说，`git switch` 是在 Git 2.23 版本中引入的命令，旨在专注于开发过程和分支之间的切换。而 `git checkout` 则是 Git 的早期命令，其不仅可以用于分支切换，还在 Git 早期，被经常用于恢复工作区到某个特定的提交状态。
- 2. 从发展过程来说，Git 早期命令比较杂乱，不方便使用。随着版本的演变，遵循单一职责原则，将 `git checkout` 进行了拆分，引入了 `git reset`、`git switch` 命令。因此，`git checkout` = `git switch` + `git reset` ，由于其功能过于混杂，平常不再推荐使用。

### 3.3.5 git merge（合并）

```
$ git merge --help | -h
## NAME
将目标分支的提交历史合并到当前分支。

## DESCRIPTION
它通过在当前分支中创建一个新的提交，然后将目标分支的所有提交应用到这个新的提交上来，完成合并。

不鼓励使用非琐碎的未提交更改来运行 git merge：虽然可能，但它可能会使您在发生冲突时处于难以退出的状态。
即运行 git merge 之前，应该先提交本地所有的更改内容，以避免冲突。

所谓冲突，指的是如果你在两个不同的分支中，对同一个文件的同一个部分进行了不同的修改，Git 就没法干净的合并它们而发生冲突问题。

## OPTIONS
$ git merge <branch>
		--abort  # 中止当前冲突解决过程，并尝试重建合并前的状态。如果合并开始时存在未提交的工作树更改，git merge --abort 在某些情况下将无法重建这些更改。因此，建议在运行 git merge之前始终提交或隐藏更改。git merge --abort 等效于存在 MERGE_HEAD 时的 git reset --merge。
		--continue  # 由于冲突而停止后，您可以通过运行 git merge --continue 来结束合并。
		--no-commit  # 在创建合并提交之前执行合并并停止，延迟生成合并提交，以便用户有机会在提交之前检查和进一步调整合并结果。即不自动创建一个新的提交，通常用于将多个小的提交合并成一个大的提交。
		--squash  # 接受被合并的分支上的所有工作，并将其压缩至一个变更集，使仓库变成一个真正的合并发生的状态，而不会真的生成一个合并提交。 这意味着你的未来的提交将会只有一个父提交，并允许你引入另一个分支的所有改动，然后在记录一个新提交前做更多的改动。
		--ff | --no-ff -m <msg> | --ff-only  
			# fast-forward | fast-forward-only，快进或仅快进模式，尽可能将合并解析为快进（只更新分支指针以匹配合并的分支；不要创建合并提交）。如果不可能（当合并的历史记录不是当前历史记录的后代时），创建合并提交或拒绝合并以非零状态退出。
			# no-fast-forward，非快进模式，在所有情况下都创建合并提交，即使合并可以作为快进解决。
			### 二者的区别：
				快进模式中，不会创建合并提交，只更新源分支指针指向被合并分支的最新提交，所以只能看到有合并到源分支的记录，但是看不到被合并分支的历史提交记录，就像在源分支中直接提交的一样。
				非快进模式中，在源分支上创建一个合并提交，然后指向被合并分支的最新提交，所以可以完整的保留被合并分支的提交记录。

## EXAMPLES
1）获取远程 origin 仓库的 develop 分支的更新，并将其合并到当前分支
$ git merge origin/develop

2）合并 feature 分支到当前分支，并将所有提交压缩成一个新的提交
$ git merge feature --squash
```

### 3.3.6 git rebase（变基）

```
$ git rebase --help | -h
## NAME
所谓变基，本质上就是将当前分支的提交历史应用到目标分支。

## DESCRIPTION
git rebase （变基）与 git merge （合并）之间的异同点：
1. 相同点（结果一致）：无论是通过变基，还是通过三方合并，整合的最终结果所指向的快照始终是一样的，只不过提交历史不同罢了。
2. 不同点：变基是将一系列提交按照原有次序依次应用到另一分支上，而合并是把最终结果合在一起。变基使得提交历史更加整洁。 你在查看一个经过变基的分支的历史记录时会发现，尽管实际的开发工作是并行的， 但它们看上去就像是串行的一样，提交历史是一条直线没有分叉。
3. 变基会丢弃一些提交历史，使提交历史变得更整洁，而合并会保留所有提交历史，但会产生分叉，不好查阅。
4. 到底合并还是变基好？这并没有一个固定的答案。

注意，如果提交存在于你的仓库之外，而别人可能基于这些提交进行开发，那么千万不要执行变基。如果你只对不会离开你电脑的提交执行变基，那就不会有事。 如果你对已经推送过的提交执行变基，但别人没有基于它的提交，那么也不会有事。 如果你对已经推送至共用仓库的提交上执行变基命令，并因此丢失了一些别人的开发所基于的提交， 那你就有大麻烦了，你的同事也会因此鄙视你。如果你或你的同事在某些情形下决意要这么做，请一定要通知每个人执行 git pull --rebase 命令，这样尽管不能避免伤痛，但能有所缓解。

总的原则是，只对尚未推送或分享给别人的本地修改执行变基操作清理历史， 从不对已推送至别处的提交执行变基操作，这样，你才能享受到两种方式带来的便利。

## OPTIONS
$ git rebase
		## 默认为当前分支
		<dst>  # 将当前分支的提交历史应用到 <dst> 分支。
		--abort  # 中止当前的 rebase 操作
		--continue  # 继续执行 rebase 操作，即使有冲突也继续。
		--skip  # 跳过当前提交，不将其应用到目标分支上。
		--root  # 以根提交（初始提交）作为基准点进行 rebase。
		-i  # 交互式的执行 rebase，允许你在合并过程中进行编辑。
		## 显式指定分支
		--onto <dst> <src>  # 将 <src> 分支的提交历史应用到 <dst> 分支
		--onto <dst> <src_1> <src_2>  # 找出 <src_2> 与 <src_1> 之间的提交历史差异，应用到 <dst> 分支

## EXAMPLES
1）将当前分支的所有提交历史应用到 bak 分支
$ git rebase bak
$ git switch bak
$ git merge <cur_branch>  # 注意，变基后需要手动执行合并更新，让 bak 分支指向 rebase 后的最新提交

2）将 client 分支的所有提交历史应用到 master 分支
$ git rebase master client
$ git switch master
$ git merge client  # 变基后需要手动执行合并更新，让 master 分支指向 rebase 后的最新提交

3）将 client 分支与 server 分支之间的提交历史差异应用到 master 分支
$ git rebase master server client
$ git switch master
$ git merge client  # 同上
```

### 3.3.7 git cherry & cherry-pick（拣选）

#### 3.3.7.1 git cherry

```
$ git cherry --help | -h
## NAME
查找尚未应用于上游的提交。

## OPTIONS
$ git cherry -v [<upstream> <head>]
		-v  # 在 SHA1 旁边显示提交主题。
		--abbrev  # 缩减版显示对象 SHA1 值
		<upstream>  # 上游分支，默认为 HEAD 的上游分支。
		<head>  # 工作分支，默认为 HEAD。

## EXAMPLES
$ git status
On branch main
Your branch is ahead of 'origin/main' by 5 commits.
  (use "git push" to publish your local commits)

接下来，我想看看当前分支的哪 5 个提交未应用于 origin/main 分支。
$ git cherry -v --abbrev
+ 428f772 update
+ 39409ac update
+ 724d253 update
+ 835864d add new file: test.txt
+ 19e9376 patch: test.txt, 244.md
```

#### 3.3.7.2 git cherry-pick

```
$ git cherry-pick --help | -h
## NAME
应用某些现有提交所引入的更改（到当前分支）。

## DESCRIPTION
给定一个或多个现有提交，应用每个提交引入的更改，为每个提交记录一个新的提交。这需要您的工作树是干净的（没有来自 HEAD 提交的修改）。

Git 中的拣选类似于对特定的某次提交的变基。 它会提取该提交的补丁，之后在当前分支创建一个新的提交，并将其应用到当前分支上。这种方式在你只想引入主题分支中的某个提交，或者主题分支中只有一个提交，而你不想运行变基时很有用。

## EXAMPLES
$ git cherry-pick e43a6  # 引入 e43a6 提交对象与当前分支最新提交对象之间的更新差异，然后在当前分支生成一个新的提交。
Finished one cherry-pick.
[master]: created a0a41a9: "More friendly message when locking the index fails."
 3 files changed, 17 insertions(+), 3 deletions(-)
```

### 3.3.8 git rerere

```
$ git rerere --help | -h
## NAME
reuse recorded resolution，重用已记录的冲突解决方案。

## DESCRIPTION
在使用寿命相对较长的主题分支的工作流中，开发人员有时需要反复解决相同的冲突，直到主题分支完成（要么合并到 “发布” 分支，要么发送到上游并接受）。
此命令通过在初始手动合并中记录冲突的自动合并结果和相应的手动解决结果，并将以前记录的手动解决方案应用于相应的自动合并结果，来帮助开发人员完成此过程。

## NOTES
您需要设置配置变量 rerere.enabled 才能启用此命令。

通常，rerere 在没有参数或用户干预的情况下自动运行。如果你需要和 rerere 的缓存交互，你可以使用 git rerere 命令。当单独调用它时，Git 会检查解决方案数据库，尝试寻找一个和当前任一冲突相关的匹配项并解决冲突。 它也有若干子命令，可用来查看记录项，删除特定解决方案和清除缓存全部内容等。

## SUBCOMMANDS
$ git rerere
		clear  # 清除 rerere 缓存的全部内容。
		forget <pathspec>  # 删除特定 <pathspec> 的冲突解决方案。
		diff  # 显示冲突解决方案的差异。
		status  # 打印具有冲突的路径，重新读取合并解决方案将记录这些冲突。
		remaining  # 打印具有未通过自动解决方案的冲突的路径。
		gc  # 删除很久以前发生的冲突合并的记录，默认情况下，会删除超过 15 天的未解决冲突和超过 60 天的已解决冲突。
```

### 3.3.9 git pull

```
$ git pull --help | -h
## NAME
从远程仓库拉取（git fetch）目标分支的提交更新并合并（git merge）到本地分支。

## DESCRIPTION
如果当前分支位于远程之后，则默认情况下，它将快进（fast forward）当前分支以匹配远程。

如果当前分支和远程分支已经分叉，则用户需要指定如何使用 --rebase 或 --no-rebase（或 pull.rebase 中的相应配置选项）协调分叉分支。

更准确地说，git pull 使用给定的参数运行 git fetch，然后根据配置选项或命令行标志，调用 git rebase 或 git merge 来协调分支。

另外，我们不鼓励使用未提交的更改运行 git pull，即运行 git pull 之前，应该先提交本地所有的更改内容，以避免冲突。

## OPTIONS
$ git pull <remote> <branch> 
		--rebase  # 在拉取更新后，将本地分支的提交重新应用到远程分支上。
		--ff-only  # 强制使用快速合并（Fast-Forward），即使有冲突也继续。
		--no-edit  # 不进入交互编辑模式，直接完成合并。

## EXAMPLES
1）将远程仓库 main 分支的最新更新合并到本地的分支上
$ git pull origin main

2）基于1），拉取更新，合并成功后，将本地分支的提交历史应用到远程分支上。
$ git pull origin main --rebase
```

### 3.3.10 git push

```
$ git push --help | -h
## NAME
将本地分支的提交历史推送到远程仓库。

## DESCRIPTION
使用本地引用更新远程引用，同时发送完成给定引用所需的对象。

## OPTIONS
$ git push <remote>
		## 推送分支
		--all  # 推送所有分支（即 refs/heads/* ）；不能与其他 <refspec> 一起使用。
		<refspec>  # 推送满足 <refspec> 规则的分支。
		<branch> -u | --set-upstream  # 对于每个最新或成功推送的分支，添加上游（跟踪）引用。
		--prune  # 删除没有本地对应分支的远程分支。例如，如果具有相同名称的本地分支不再存在，则远程分支 tmp 将被删除。这也尊重 <refspecs> ，例如 git push remote --prune refs/heads/*:refs/tmp/* 将确保在 refs/heads/soo 不存在的情况下删除远程 refs/tmp/foo。
		-d | --delete  # 所有列出的引用都将从远程存储库中删除。这与在所有引用前面加一个冒号（:<dst>）是一样的。
		## 推送标签
		--tags  # 推送所有标签到远程仓库。
		<tagname>  # 推送指定名称的标签到远程仓库。
		:refs/tags/<tagname> | --delete <tagname>  # 移除远程仓库中名称为 tagname 的标签

1）将本地 master 分支的更改推送到远程 origin 仓库的 master 分支
$ git push origin master  # = git push origin refs/heads/master:refs/heads/master

2）将本地 matser 分支的更改推送到远程 origin 仓库的 develop 分支，如果远程 develop 仓库不存在，则创建。
$ git push origin master:refs/heads/develop

3）将本地所有的标签推送到远程仓库
$ git push origin --tags

4）移除远程仓库中的标签
$ git push origin :refs/tags/<tagname>
```

## 3.4 git tag（标记）

```
$ git tag --help | -h
## NAME
创建、列出、删除或验证使用 GPG 签名的标记对象。

## DESCRIPTION
Git 标签分为轻量标签和附注标签。
1. 所谓轻量标签，指的是仓库历史中的某一个对象（一般指提交对象，也可以是数据对象、树对象）的命名简称。
2. 所谓附注标签，指的是将仓库历史中的某一个对象的校验和，以及一些标签附加信息（打标签者的名字、电子邮件地址、日期时间）组织在一起生成的一个标签对象。
通常我们建议创建附注标签，这样你可以拥有以上所有信息。但是如果你只是想用一个临时的标签， 或者因为某些原因不想要保存这些信息，那么也可以用轻量标签。

## OPTIONS
$ git tag
		-l | --list  # 默认选项。如果不写，默认返回所有的标签列表；如果存在，则需要提供一个匹配标签名的通配模式。
		<tagname> [obj-sha1] # 为指定对象创建一个轻量标签，默认为当前提交对象。
		-a <tagname> [obj-sha1] & -m '说明信息' # 为指定对象创建一个附注标签，默认为当前提交对象。
		-s <tagname> [obj-sha1] & -m '说明信息'  # 使用默认电子邮件地址的密钥制作一个 GPG 签名的标签。标记 GPG 签名的默认行为由 tag.gpgSign 配置变量控制（如果存在），否则禁用。
		-d <tagname>  # 删除本地仓库上的标签，该命令并不会从任何远程仓库中移除这个标签，你必须使用 git push <remote> :refs/tags/<tagname> | git push <remote> --delete <tagname> 来更新你的远程仓库。

## EXAMPLES
1）查看本地仓库中所有的标签
$ git tag
v1.0
v2.0

2）查看满足匹配模式的标签
$ git tag -l "v1.8.5*"
v1.8.5
v1.8.5-rc0
v1.8.5-rc1
v1.8.5.1

3）为当前提交对象创建一个轻量标签
$ git tag v1.0

4）为当前提交对象创建一个附注标签
$ git tag -a v1.0.0 -m 'version 1.0.0'

5）删除一个本地标签，并同步至远程仓库
$ git tag -d v1.0.0
$ git push origin :refs/tags/v1.0.0

6）查看标签的详细信息，如果是轻量标签，仅显示提交对象信息；如果是附注标签，将显示标签对象和提交对象信息。
$ git show v1.0
```

## 3.5 git describe（生成对象名称）

```
$ git describe --help | -h
## NAME
根据可用引用为（提交）对象指定一个可读的名称。

## DESCRIPTION
该命令查找可从提交中访问的最新标记。如果标记指向提交，则只显示标记。否则，它会在标记名称后面加上标记对象上的附加提交次数和最近提交的缩写对象名称。结果是一个 “人类可读” 的对象名称，它也可以用于识别对其他 git 命令的提交。

## EXAMPLES
$ git describe parent
v1.0.4-14-g2414721  # 我的 “parent” 分支的当前头基于 v1.0.4 标签，但由于它还有一些提交，describe 在末尾添加了额外提交的数量（“14”）和最近一次提交的缩写对象名称（“2414721”）。
```

## 3.6 git archive（生成快照归档）

```
$ git archive --help | -h
## NAME
创建一个最新的快照归档。

## DESCRIPTION
创建指定格式的存档，其中包含命名树的树结构，并将其写入标准输出。如果指定了 --prefix=<folder>，则会将其置于存档中的文件名之前。
当给定 tree ID 时，git archive 的行为与给定 commit ID 或 tag ID 时不同。
1. 对于 tree ID，当前时间用作存档中每个文件的修改时间。
2. 对于 commit ID 或 tag ID，将使用引用的提交对象中记录的提交时间。
此外，如果使用 tar 格式，则 commit ID 存储在全局扩展的 pax header 中；它可以使用 git get-tar-commit-id 来提取。在 ZIP 文件中，它存储为文件注释。

## OPTIONS
$ git archive <branch | object ID>
		-l | --list  # 显示所有可用的格式。
		--prefix=<folder>/  # 将 forder/ 前置到存档中的路径。可以重复；其最右边的值用于所有被跟踪的文件。
		--format=<tar | zip | tar.gz | tgz>  # 指定生成的存档格式，默认为 tar.gz。

## EXAMPLES
1）创建一个最新的快照归档，并使用 gzip 生成压缩文件
$ git archive master --prefix='project/' | gzip > `git describe master`.tar.gz
$ ls *.tar.gz
v1.6.2-rc1-20-g8c5b85c.tar.gz

2）创建一个最新的快照归档，并指定使用 zip 生成压缩文件
$ git archive master --prefix='project/' --format=zip > `git describe master`.zip
$ ls *.zip
v1.6.2-rc1-20-g8c5b85c.zip
```
# 4、git status

```
$ git status --help | -h
## NAME
显示工作树的状态（文件的更改摘要）。

## DESCRIPTION
显示工作树的状态（未跟踪 | 已跟踪）：
	1）【未跟踪（Untracked，100000）】：Git 未跟踪的工作树中的路径（git ignore（5）未忽略的这些路径）；
	2）【未修改（Unmodified） | 已修改（Modified） | 已删除（deleted）】：工作树和索引文件之间存在差异的路径；
	3）【已暂存（Staged）】：索引文件和当前 HEAD 提交之间存在差异的路径。
第 1）个和第 2）个是您通过运行 git add 来暂存的工作区的内容，第 3）个是您将通过运行 git commit 来提交的暂存区的内容，第 4）个是您已运行 git commit 提交之后本地仓库的内容。

## OPTIONS
$ git status
		-s | --short  # 以简短的格式给出输出。

## EXAMPLES
1）显示工作树的各种状态（文件的更改摘要）
$ git status
On branch main
Your branch is up-to-date with 'origin/master' by 2 commits.  # 本地仓库中所有未发布的文件清单
  (use "git push" to publish your local commits)

Changes to be committed:  # 暂存区中所有未提交的文件清单
  (use "git reset HEAD <file>..." to unstage)
	new file:   README
	modified:   CONTRIBUTING.md
	deleted:    PROJECTS.md
	renamed:    README.md -> README

Changes not staged for commit:  # 工作区中所有未暂存的文件清单
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
    modified:   CONTRIBUTING.md
    deleted:    PROJECTS.md

Untracked files:  # 未跟踪的文件清单
  (use "git add <file>..." to include in what will be committed)
    README

2）以简洁的方式查看工作树的各种状态
$ git status -s
【简短输入内容格式】
X[暂存区]Y[工作区] PATH[文件路径] | ORIG_PATH -> PATH[移动文件路径]，其中XY的取值如下：
	|- !：ignored，已忽略
	|- ?：untracked，未跟踪
	|- 空格：unmodified，未修改
	|- A：added，已添加
	|- M：modified，已修改
	|- D：deleted，已删除
	|- U：updated but unmerged，已更新，但未合并
	|- C：copied，已拷贝
	|- R：renamed，已移动
	|- T：file type changed (regular file, symbolic link or submodule)，文件类型已更改（常规文件、符号链接或子模块）
【输出样例】
 M README
MM Rakefile
A  lib/git.rb
M  lib/simplegit.rb
?? LICENSE.txt
```

# 5、vim .gitignore

一般我们总会有些文件无需纳入 Git 的管理，也不希望它们总出现在未跟踪文件列表。 通常都是些自动生成的文件，比如日志文件，或者编译过程中创建的临时文件等。 在这种情况下，我们可以创建一个名为 `.gitignore` 的文件，列出要忽略的文件的模式。

## 5.1 编写格式规范

- 首先，`.gitignore` 文件中的所有空行或以 `#` 开头（注释内容）的行都不会被作为忽略规则去执行。
- 可以使用标准的 glob 模式进行匹配 ，它会递归地应用在整个工作区中。所谓的 glob 模式是指 shell 所使用的简化了的正则表达式，例如：
	- `[a-z0-9A-Z]`：匹配任何一个列在方括号中的字符 （这个例子要么匹配一个 a-z，要么匹配一个 0-9，要么匹配一个 A-Z）
	- `[?]`：匹配一个任意字符；
	- `*`：匹配零个或多个任意字符；
	- `**`：匹配任意中间目录；
	- ... ...
- 可以使用（`/`）开头防止递归。
- 可以使用（`/`）结尾指定目录。
- 可以在模式前加上叹号（`!`）取反，表示除外，不忽略。

## 5.2 编写案例

[.gitignore](https://github.com/github/gitignore) 给出了常用的 `.gitignore` 文件列表，可以参考。

```
# 忽略 README.md 文件
README.md

# 忽略所有的 .a 文件
*.a
# 跟踪所有的 lib.a 文件，即便你在前面忽略了所有的 .a 文件
!lib.a

# 只忽略当前目录下的 TODO 文件，而不忽略 subdir/TODO 文件
/TODO

# 忽略任何目录下名为 build 的文件夹
build/

# 忽略 doc/notes.txt 文件，但不忽略 doc/server/arch.txt 文件
doc/*.txt

# 忽略 doc/ 目录及其所有子目录下的 .pdf 文件
doc/**/*.pdf
```

# 6、git diff

```
$ git diff --help | -h
## NAME
显示工作（区）树文件与（暂存区）索引之间、索引与（存储库）当前 HEAD 之间的变化内容。

## DESCRIPTION
显示工作树与索引或树之间的更改、索引与树之间的变更、两个树之间的变化、合并引起的变化、两个blob对象之间的变化或磁盘上两个文件之间的变化。
1. git diff <options> -- <path>  # （默认）查看相对于索引（下一次提交的暂存区域）所做的更改。换句话说，这些差异是你可以告诉 Git 进一步添加到索引中的，但你仍然没有。您可以使用 git add 来暂存这些更改。
2. git diff <options> <blob>  # （默认）查看相对于索引（下一次提交的暂存区域）所做的更改。
3. git diff oldref...newref  #  显示自当前 newref 分支与 oleref 分支的共同祖先起，newref 分支中的所添加的更改。这个语法很有用，应该牢记。

## OPTIONS
$ git diff
	<options>
		-p | -u | --patch  # 生成修补程序，默认选项。
		--cached | staged  # 显示索引与 HEAD 之间的变化，默认为显示工作树文件与索引之间的变化。
		--shortstat  # 只输出 --stat 格式的最后一行，其中包含修改的文件总数以及添加和删除的行数。
		--name-status  # 仅显示更改文件的名称和状态。
		--check  # 如果更改引入冲突标记或空白错误，则发出警告。所谓的空白错误是由 core.whitespace 配置控制的。默认情况下，尾随空格（包括仅由空格组成的行）和行的初始缩进内紧跟制表符的空格字符被视为空格错误。如果发现问题，则以非零状态退出。
	-- <path>  # 同 1. 上
	<blob>  # 同 2. 上
	oldref...newref  # 同 3. 上
	

## EXAMPLES
1）显示工作树与索引之间的更改内容
$ git diff
diff --git a/demo.txt b/demo.txt  # 这是 diff 命令，用于比较两个文件的差异。--git 选项表示只显示与 Git 相关的信息。a/demo.txt 和 b/demo.txt 是要比较的两个文件的路径。
index e16d301..f4548dc 100644  # 这是文件的索引信息，包括文件的哈希值和状态。在这个例子中，工作区文件的哈希值是 e16d301，暂存区文件的哈希值是 f4548dc，文件模式为普通文件（100644）。
--- a/demo.txt  # 旧版本文件路径。
+++ b/demo.txt  # 新版本文件路径。
@@ -13,17 +13,15 @@ 12 advice.graftFileDeprecated  # 这是 diff 命令的输出格式，用于显示实际的差异内容。-13,17 表示旧版本的文件从 13 行开始，有 17 行；+13,15 表示新版本的文件从 13 行开始，有 15（-5，+3，少 2） 行。advice.graftFileDeprecated 内容为对比开始前一行的内容。
13 advice.ignoredHook
14 advice.implicitIdentity
15 advice.nameTooLong
16 -advice.nestedTag  # 以下 5 行代码是在旧版本中删除（-）的。
17 -advice.objectNameWarning  
18 -advice.pushAlreadyExists
19 -advice.resetNoRefresh
20 -advice.resolveConflict
21 advice.rmHints
22 advice.sequencerInUse
23 advice.setUpstreamFailure
24 advice.skippedCherryPicks
25 advice.statusAheadBehindWarning
26 advice.statusHints
27 +aaa  # 以下 3 行代码是在新版本中添加（+）的。
28 +bbb
29 +ccc
30 advice.statusUoption
31 advice.submoduleAlternateErrorStrategyDie

2）显示索引与 HEAD 之间的更改内容。
$ git diff --cached
diff --git a/CONTRIBUTING.md b/CONTRIBUTING.md
index 8ebb991..643e24f 100644  # 这是文件的索引信息，包括文件的哈希值和状态。在这个例子中，暂存区文件的哈希值是 e16d301，本地仓库文件的哈希值是 f4548dc，文件模式为普通文件（100644）。
--- a/CONTRIBUTING.md
+++ b/CONTRIBUTING.md
@@ -65,7 +65,8 @@ 64 branch directly, things can get messy.  # 这是 diff 命令的输出格式，用于显示实际的差异内容。-65,7 表示旧版本的文件从 65 行开始，有 7 行；+65,8 表示新版本的文件从 65 行开始，有 8（-1，+2，多 1） 行。branch directly, things can get messy. 内容为对比开始前一行的内容。
65 Please include a nice description of your changes when you submit your PR;
66 if we have to read the whole diff to figure out why you're contributing
67 in the first place, you're less likely to get feedback and have your change
68 -merged in.
69 +merged in. Also, split your changes into comprehensive chunks if your patch is
70 +longer than a dozen lines.
71 
72 If you are starting to work on a particular area, feel free to submit a PR
73 that highlights your work in progress (and note in the PR title that it's

3）显示当前主题分支与 master 分支的共同祖先起，该分支所添加的更改
$ git diff master...topic-a  # 这个语法很有用，应该牢记。
```

# 7、git 补丁

## 7.1 生成补丁

### 7.1.1 使用 git diff 生成补丁

默认情况下， `git diff` 的输出即为标准的补丁内容。

```
$ git diff -- test.txt
diff --git a/test.txt b/test.txt
index 8009d00..e698998 100644
--- a/test.txt
+++ b/test.txt
@@ -1 +1,2 @@
 test...
+test222...
```

> 说明：
> 如果可能的话，请鼓励贡献者使用 `format-patch` 而不是 `diff` 来为你生成补丁。这样的话你的工作会变得更加轻松，因为这种补丁中包含了作者信息和提交信息供你参考。
### 7.2.2 使用 format-patch 生成（mbox格式的）补丁

使用 `git format-patch` 来生成可以邮寄到列表的 mbox 格式的文件——它将每一个提交转换为一封电子邮件，提交信息的第一行作为主题，剩余信息与**提交引入的补丁**作为正文，即使用 `format-patch` 生成的一封电子邮件应用的提交正确地保留了所有的提交信息。

```
## NAME
准备用于提交电子邮件的修补程序。

## DESCRIPTION
在每次提交的一条 “消息” 中准备每个非合并提交及其 “补丁” ，其格式类似于 UNIX 邮箱。此命令的输出便于电子邮件提交或与 git am 一起使用。
命令生成的 “消息” 由三部分组成：
1. 一个简短的元数据头，以 From <commit> 开头，带有固定的 2001 年 9 月 17 日星期一 00:00:00 日期戳，以帮助 git patch | am 等程序识别该文件是该命令的输出，以及记录作者身份、作者日期和更改标题的字段（取自提交日志消息的第一段）。
2. 提交日志消息的第二段及后续段落。
3. 补丁内容，即提交与其父级之间的 diff -p --stat 输出。

## OPNTIONS
$ git format-patch
		<options>
			-M | --find-renames # 检测重命名。如果指定了 n，则它是相似性指数的阈值（即，与文件大小相比的添加 / 删除量）。例如，-M90% 意味着如果超过 90% 的文件没有更改，Git 应该将删除 / 添加对视为重命名。
		<branch>  # 目标分支，即（当前分支）基于目标分支（二者的差异）创建补丁内容。

## EXAMPLES
$ git format-patch -M origin/master  # （当前分支）基于 origin/master 分支（二者的差异）创建补丁内容
0001-add-limit-to-log-function.patch
0002-changed-log-output-to-30-from-25.patch

$ cat 0001-add-limit-to-log-function.patch
======================================================================
### 1. 一个简短的元数据头，以 From <commit> 开头，带有固定的 2001 年 9 月 17 日星期一 00:00:00 日期戳，以帮助 git patch | am 等程序识别该文件是该命令的输出，以及记录作者身份、作者日期和更改标题的字段（取自提交日志消息的第一段）。
From 330090432754092d704da8e76ca5c05c198e71a8 Mon Sep 17 00:00:00 2001
======================================================================
### 2. 提交日志消息的第二段及后续段落。
From: Jessica Smith <jessica@example.com>
Date: Sun, 6 Apr 2008 10:17:23 -0700
Subject: [PATCH 1/2] add limit to log function

Limit log functionality to the first 20

======================================================================
### 3. 补丁内容，即提交与其父级之间的 diff -p --stat 输出。
---
### 3.1 diff --stat 输出的内容，如果在 `---` 行与补丁开头（`diff --git` 行）之间添加文本，那么开发者就可以阅读它，但是应用补丁时会忽略它。
 lib/simplegit.rb |    2 +-
 1 files changed, 1 insertions(+), 1 deletions(-)

### 3.2 diff -p 输出的内容
diff --git a/lib/simplegit.rb b/lib/simplegit.rb
index 76f47bc..f9815f1 100644
--- a/lib/simplegit.rb
+++ b/lib/simplegit.rb
@@ -14,7 +14,7 @@ class SimpleGit
   end

   def log(treeish = 'master')
-    command("git log #{treeish}")
+    command("git log -n 20 #{treeish}")
   end

   def ls_tree(treeish = 'master')
--
2.1.0
```

### 7.1.3 使用 git send-email 发送补丁

```
## NAME
以电子邮件形式发送修补程序集合。

## DESCRIPTION
获取命令行上提供的补丁并通过电子邮件发送出去。修补程序可以指定为文件、目录（将发送目录中的所有文件），也可以直接指定为修订列表。
修补程序文件可接受两种格式：
1. mbox 格式的文件。这就是 git format-patch 生成的内容。
2. 忽略。

## EXAMPLES
$ git send-mail *.patch  # 发送前需使用 git config 配置 SMTP 邮箱信息
0001-added-limit-to-log-function.patch
0002-changed-log-output-to-30-from-25.patch
Who should the emails appear to be from? [Jessica Smith <jessica@example.com>]
Emails will be sent from: Jessica Smith <jessica@example.com>
Who should the emails be sent to? jessica@example.com
Message-ID to be used as In-Reply-To for the first email? y

然后，对于正在发送的每一个补丁，Git 会吐出这样的一串日志信息：
(mbox) Adding cc: Jessica Smith <jessica@example.com> from
  \line 'From: Jessica Smith <jessica@example.com>'
OK. Log says:
Sendmail: /usr/sbin/sendmail -i jessica@example.com
From: Jessica Smith <jessica@example.com>
To: jessica@example.com
Subject: [PATCH 1/2] added limit to log function
Date: Sat, 30 May 2009 13:29:15 -0700
Message-Id: <1243715356-61726-1-git-send-email-jessica@example.com>
X-Mailer: git-send-email 1.6.2.rc1.20.g8c5b.dirty
In-Reply-To: <y>
References: <y>

Result: OK
```

## 7.2 应用补丁

### 7.2.1 使用 git apply 应用补丁

```
## NAME
将修补程序应用于工作目录中的文件和 / 或索引。

## DESCRIPTION
读取提供的 diff 输出（即 “补丁” ）并将其应用于工作目录中的文件。从存储库中的子目录运行时，会忽略目录外的修补路径。使用 --index 选项时，补丁也应用于索引，使用 --cached 选项时，修补程序仅应用于索引。如果没有这些选项，该命令只将补丁应用于文件，而不要求它们位于 Git 存储库中。

## README
此命令应用修补程序，但不创建提交，即在运行之后，你需要手动暂存并提交补丁所引入的更改。使用 git-am 从 git format-patch 生成的和 / 或通过电子邮件接收的补丁中创建提交。

如果可能的话，请鼓励贡献者使用 git format-patch 而不是 git diff 来为你生成补丁。而且只有对老式的补丁，你才必须使用 git apply 命令。

## OPTIONS
$ git apply
		<options>
			--summary  # 不应用补丁，而是输出从 git diff 扩展头中获得的信息的浓缩摘要，如创建、重命名和模式更改。
			--check  # 检查补丁是否可以顺利应用，如果没有产生输出，则该补丁可以顺利应用。 如果检查失败了，以一个非零的状态退出。
			--index  # 修补程序同时应用于工作目录中的文件和索引中。
			--cached  # 修补程序仅应用于索引。
			-3 | --3way  # 如果补丁记录了它应该应用到的 Blob 的标识，并且我们在本地有这些 Blob 可用，则尝试三向合并，可能会将冲突标记留在工作树中的文件中供用户解决。此选项意味着 --index选项，除非使用了 --cached 选项。
		<patch-file>  # 修补程序文件路径

## EXAMPLES
$ git apply --check 0001-update.patch  # 检查补丁文件 0001-update.patch 是否可以成功应用于工作目录中的文件。
error: patch failed: 云原生之路（精通）/VCS/Git/Git常用命令.md:1233  # 修补程序应用到了目标文件的 1233 行，但是这个位置的内容和补丁的预期内容不匹配，导致补丁无法成功应用。
error: 云原生之路（精通）/VCS/Git/Git常用命令.md: patch does not apply  # 修补程序无法应用到目标文件，因为这个位置的内容和补丁的预期内容不匹配。

### 如果没有产生输出，则该补丁可以顺利应用。
$ git apply 0001-update.patch
```

### 7.2.2 使用 git am 应用补丁

```
## NAME
从邮箱应用一系列修补程序。

## DESCRIPTION
将邮箱中的邮件拆分为提交日志消息、作者信息和修补程序，并将它们应用于当前分支，创建一个新的提交，其中的作者信息来自于电子邮件头部的 From 和 Date 字段，提交消息则取自 Subject 和邮件正文中补丁之前的内容。

## OPTIONS
$ git am 
		<options>
			--resolved  # 在补丁失败后（例如，试图应用冲突的补丁），用户已经手动应用了它，并且索引文件存储了应用的结果。使用从电子邮件和当前索引文件中提取的作者身份和提交日志进行提交，然后继续。
			--abort  # 恢复原始分支并中止修补操作。将 am 操作中涉及的文件的内容恢复到其 am 前状态。
			-3 | --3way # 如果补丁记录了它应该应用到的 Blob 的标识，并且我们在本地有这些 Blob 可用，则尝试三向合并，可能会将冲突标记留在工作树中的文件中供用户解决（手动编辑那些文件来解决冲突，暂存新的文件， 之后运行 git am --resolved 继续应用下一个补丁）。
			-i | --interactive  # 以交互方式运行。
		< mbox 格式的补丁文件>

## EXAMPLES
$ git am 0001-seeing-if-this-helps-the-gem.patch
Applying: seeing if this helps the gem
error: patch failed: ticgit.gemspec:1
error: ticgit.gemspec: patch does not apply
Patch failed at 0001.
When you have resolved this problem run "git am --resolved".
If you would prefer to skip this patch, instead run "git am --skip".
To restore the original branch and stop patching run "git am --abort".

该命令将会在所有出现问题的文件内加入冲突标记，就和发生冲突的合并或变基操作一样。 而你解决问题的手段很大程度上也是一样的——即手动编辑那些文件来解决冲突，暂存新的文件， 之后运行 git am --resolved 继续应用下一个补丁：

$ (fix the file)
$ git add ticgit.gemspec
$ git am --resolved
Applying: add limit to log function

你会看到补丁被顺利地应用，并且为你自动创建了一个新的提交。 其中的作者信息来自于电子邮件头部的 From 和 Date 字段，提交消息则取自 Subject 和邮件正文中补丁之前的内容。

$ git log --pretty=fuller -1
commit 6c5e70b984a60b3cecd395edd5b48a7575bf58e0
Author:     Jessica Smith <jessica@example.com>
AuthorDate: Sun Apr 6 10:17:23 2008 -0700
Commit:     Scott Chacon <schacon@gmail.com>
CommitDate: Thu Apr 9 09:19:06 2009 -0700

   add limit to log function

   Limit log functionality to the first 20
```

# 8、git add/rm/mv

## 8.1 git add

```
## NAME
将文件内容添加到索引（暂存区）。

## DESCRIPTION
此命令使用工作树中的当前内容更新索引，为下一次提交准备内容。它通常将现有路径的当前内容作为一个整体添加，但通过某些选项，它也可以用于添加仅应用对工作树文件所做的部分更改的内容，或者删除工作树中不再存在的路径。

“索引” 保存工作树内容的快照，正是此快照被用作下一次提交的内容。因此，在对工作树进行任何更改之后，在运行 commit 命令之前，必须使用 add 命令将任何新的或修改过的文件添加到索引中。

此命令可以在提交之前执行多次。它只在运行 add 命令时添加指定文件的内容；如果您希望在下一次提交中包含后续更改，则必须再次运行 git add 以将新内容添加到索引中。

git add 命令默认情况下不会添加被忽略的文件。如果在命令行上明确指定了任何被忽略的文件，git add 将失败，并显示被忽略文件的列表。Git 执行的目录递归或文件名 globbing（在 shell 之前引用 globs ）到达的被忽略文件将被静默忽略。

git add 命令可用于通过 -f（force）选项添加被忽略的文件。

## OPTIONS
$ git add
		-n | --dry-run  # 试运行，不要实际添加文件，只显示它们是否存在和/或将被忽略。
		-A | --all  # 不仅在工作树有与 ＜pathspec＞ 匹配的文件的地方更新索引，而且在索引已经有条目的地方也更新索引。这会添加、修改和删除索引项，以匹配工作树。如果在使用 -A 选项时没有给出 <pathspec>，则整个工作树中的所有文件都会更新。
		-i | interactive  # 以交互方式将工作树中修改后的内容添加到索引中。可以提供可选的路径参数，以将操作限制在工作树的子集上。
		-p | --patch  # 交互式地在索引和工作树之间选择大块补丁，并将它们添加到索引中。这让用户有机会在将修改后的内容添加到索引之前查看差异，并提交部分暂存的文件内容。

## EXAMPLES
1）将工作树文件内容添加到索引（暂存区）
$ git add Docs/\*.txt  # 添加 Docs 目录及其子目录下所有以 .txt 结尾的文件
$ git add git-*.sh  # 添加当前目录中所有 git-*.sh 脚本文件，仅限于当前目录，不包括子目录。
$ git add -A | *  # 添加所有文件到索引中。

2）交互式暂存
$ git add -i
###  暂存的修改列在左侧，未暂存的修改列在右侧，文件路径列在最后。
           staged   unstaged   path  
  1:    unchanged      +0/-1   TODO
  2:    unchanged      +1/-1   index.html
  3:    unchanged      +5/-1   lib/simplegit.rb
  
### 命令区域， 在这里你可以做一些工作，包括暂存文件、取消暂存文件、暂存文件的一部分、添加未被追踪的文件、显示暂存内容的区别等。
*** Commands ***
  1: [s]tatus     2: [u]pdate      3: [r]evert     4: [a]dd untracked
  5: [p]atch      6: [d]iff        7: [q]uit       8: [h]elp
What now> 8
status    - 显示有更改的路径，已经在第一部分列出来了
update    - 进入 update>> 模式，选择目标文件，然后按回车键将其放入暂存区，最后运行 git commit 来提交部分暂存的文件。
revert    - 撤销第二步的操作，取消已经放入暂存区的文件
patch     - 进入 update>> 模式，选择目标文件和需要暂存的更新区块，然后按回车键将其放入暂存区，最后运行 git commit 来提交部分暂存的文件内容。此外，你可以在命令行中使用 git add -p 或 git add --patch 来启动同样的脚本。
diff      - 进入 diff>> 模式，选择目标文件，显示暂存内容的区别，这跟你在命令行指定 git diff path --cached 输出相同的结果。
add untracked    - 将未跟踪文件的内容添加到暂存区。
```

## 8.2 git rm

```
$ git rm --help | -h
## NAME
从工作树和索引中删除文件。

## DESCRIPTION
从索引或工作树和索引中删除与路径规范匹配的文件。git rm 不会仅从您的工作目录中删除文件（没有只从工作树中删除文件并将其保留在索引中的选项；如果要这样做，请使用 /bin/rm）。被删除的文件必须与分支的顶端相同，并且不能在索引中暂存对其内容的更新，但可以使用 -f 选项覆盖默认行为。当提供 --cached 时，暂存的内容必须与分支的顶端或磁盘上的文件相匹配，从而允许仅从索引中删除该文件。

## OPTIONS
$ git rm 
		-n | --dry-run  # 实际上不要删除任何文件。相反，只显示它们是否存在于索引中，否则会被命令删除。
		-f | --force  # 覆盖最新检查。
		-r  # 在给定前导目录名时允许递归删除。
		--cached  # 使用此选项可以仅从索引中取消暂存和删除路径。工作树文件，无论是否修改，都将被单独保留。

## EXAMPLES
1）从工作树中删除文件
$ rm hello.txt
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
		deleted: hello.txt

2）从索引中删除文件
$ git rm hello.txt  # git rm hello.txt = rm hello.txt + git rm hello.txt，即该命令本身就是从工作树和索引中删除文件。
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
		deleted: hello.txt
```

## 8.3 git mv

```
$ git mv --help | -h
## NAME
移动或重命名文件、目录或符号链接。

## DESCRIPTION
1. git mv ＜source＞ ＜destination＞ ，将 ＜source＞ 重命名为 ＜destination＞ ，＜source＞ 必须存在并且是文件、符号链接或目录。
2. git mv ＜source＞ ＜destination directory＞ ，将 ＜source＞ 移动到 ＜destination directory＞ ，最后一个参数必须是一个现有目录；给定的源将被移到此目录中。
索引会在成功完成后更新，但仍必须提交更改。
本质上，运行 git mv 就相当于运行了下面三条命令：
$ mv hello.txt hello 或者 mv hello.txt test/
然后
$ git rm hello.txt
$ git add hello 或者 git add test/hello

## OPTIONS
$ git mv 
		-f | --force  # 强制重命名或移动文件，即使存在 ＜destination＞。
		-v | --verbose  # 报告文件移动时的名称。

## EXAMPLES
1）对文件进行重命名
$ git mv hello.txt hello
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
		renamed: hello.txt -> hello

2）将文件移动到指定目录内
$ git mv hello.txt test/
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
		renamed: hello.txt -> test/hello.txt
```

# 9、git restore

## 9.1 命令详解

```
$ git restore --help | -h
## NAME
恢复工作树文件，包括（默认）恢复工作树中的文件，（--staged --worktree）恢复工作树文件以匹配索引中的版本，以及（--source=HEAD --staged --worktree）恢复索引以及工作树中的文件以匹配 HEAD 中的版本。

## DESCRIPTION
使用还原源中的某些内容还原工作树中的指定路径。如果某个路径被跟踪但在还原源中不存在，则会将其删除以匹配该源。

该命令还可以用于使用 --staged 恢复索引中的内容，或者使用 --staged --worktree 恢复工作树和索引。

默认情况下，如果给定--staged，则从 HEAD 还原内容，否则从索引还原内容。使用 --source 从其他提交进行恢复。

## OPTIONS
$ git restore 
		-s | --source=<tree>  # 使用给定树中的内容恢复工作树文件。通常通过命名与之相关的提交、分支或引用来指定源树。如果未指定，则在给定 --staged 的情况下从 HEAD 还原内容，否则从索引还原内容。
		-S & -W | --staged & --worktree # 指定还原位置。如果两个选项都未指定，则默认情况下将恢复工作树。指定 --staged 只会恢复索引。指定两者都可以恢复两者。


## EXAMPLES
1）恢复工作树中的文件以匹配索引中的版本
$ git restore '*.c'  # 所有 C 源文件
$ git restore .  # 当前目录中的文件
$ git restore *  # 所有文件

2）恢复索引中的文件以匹配 HEAD 中的版本，以及恢复工作树中的文件以匹配索引中的版本
$ git restore --staged --worktree hello.c

3）同时恢复索引和工作树中的文件以匹配其他提交中的版本
$ git restore --source=other --staged --worktree hello.c
```

## 9.2 实战演练

### 9.2.1 恢复工作树中的文件以匹配索引中的版本（慎用）

在 Git 中任何 **已提交** 的东西几乎总是可以恢复的。 甚至那些被删除的分支中的提交或使用 `--amend` 选项覆盖的提交也可以恢复 （阅读 [数据恢复](https://git-scm.com/book/zh/v2/ch00/_data_recovery) 了解数据恢复）。 然而，**任何你未提交的东西丢失后很可能再也找不到了**。

```
第一步：修改、删除工作树中的文件
$ echo 'world' >> hello.txt  # 修改 hello.txt 文件
$ rm test.txt  # 删除 test.txt 文件
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified: hello.txt
        deleted:  test.txt

第二步：恢复工作树中的文件以匹配索引中的版本
$ git restore hello.txt test.txt
```

### 9.2.2 恢复索引中的文件以匹配 HEAD 中的版本

```
第一步：修改、删除工作树中的文件，并添加至索引
$ echo 'world' >> hello.txt
$ rm test.txt
$ git add hello.txt
$ git rm test.txt
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes to be committed:
  (use "git restore --staged <file>..." to unstaged)
		modified: hello.txt
		deleted:  test.txt

第二步：恢复索引中的文件以匹配 HEAD 中的版本
$ git restore --staged hello.txt test.txt
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified: hello.txt
        deleted:  test.txt

第三步：恢复工作树中的文件以匹配索引中的版本
$ git restore hello.txt test.txt
$ git status
On branch main
Your branch is ahead of 'origin/main'.
nothing ... ...
```

### 9.2.3 恢复索引中的文件以匹配其他提交中的版本

```
$ git restore --source=<commit_hash> -S -W hello.txt  # --source 的默认值为 HEAD（当前分支头指针）
```

# 10、git stash（贮藏已做的工作）

贮藏（stash）会处理工作目录的脏的状态——即跟踪文件的修改与暂存的改动，然后将未完成的修改保存到一个栈上， 而你可以在任何时候重新应用这些改动（甚至在不同的分支上）。

> 用途：
> 如果你想要切换分支，但是还不想要提交之前的工作，可以先贮藏起来。

```
$ git stash
		## default
		默认情况下，贮藏工作目录中所有已修改和暂存的已跟踪文件。
		## options
		-a | --all  # 贮藏工作目录中所有变化内容，包括未跟踪文件、已修改和暂存的已跟踪文件，以及被忽略的文件。
		--keep-index  # 不仅要贮藏所有已暂存的内容，同时还要将它们保留在索引中。
		-u | --include-untracked  # 同时贮藏任何未跟踪文件。
		-p | --patch  # 交互式地提示哪些改动想要贮藏、哪些改动需要保存在工作目录中。
		## subcommands
		push  # 默认值，将新的贮藏推送到栈上。
		list  # 列出所有的贮藏条目。
		branch <new branchname> <stash@{n}> # 创建一个新分支，检出贮藏工作时所在的提交（默认情况下，n = 0，即检出最近一次入栈的贮藏内容所在的提交），重新在那应用工作，然后在应用成功后丢弃贮藏。
		apply <stash@{n}>  # 将贮藏的工作重新应用到工作目录，默认情况下，n = 0，即应用最近一次入栈的贮藏内容。
			## 注意事项：
			1. 贮藏的索引将被清除，暂存区中的工作会被应用到工作目录。
			2. 可以在任一分支应用贮藏内容。
			3. 当应用贮藏时工作目录中也可以有修改与未提交的文件——如果有任何东西不能干净地应用，Git 会产生合并冲突。
			## options
			--index  # 重新应用暂存的修改，你将回到贮藏前的原样。
		drop <stash@{n}>  # 移除贮藏内容，默认情况下，n = 0，即移除最近一次入栈的贮藏内容。
		clear  # 删除所有贮藏条目。请注意，这些条目将被移除，并且可能无法恢复。

## EXAMPLES
第一步：查看工作目录的状态
$ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   hello.txt

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   test.txt

第二步：贮藏工作目录中的内容
$ git stash push hello.txt
Saved working directory and index state WIP on main: 06aa223 update 244.md

$ git stash push test.txt
Saved working directory and index state WIP on main: 06aa223 update 244.md

第三步：查看所有的贮藏条目
$ git stash list
stash@{0}: WIP on main: 06aa223 update 244.md
stash@{1}: WIP on main: 06aa223 update 244.md

第四步：创建新分支，检出贮藏工作时所在的提交，重新在那应用工作
$ git stash branch s1 stash@{0}

```

# 11、git commit

```
$ git commit --help | -h
## NAME
记录对存储库的更改。

## DESCRIPTION
创建一个新的提交，其中包含索引的当前内容和描述更改的给定日志消息。新提交是 HEAD 的直接子级，通常是当前分支的顶端，并且分支会更新为指向它。

可以通过以下几种方式指定要提交的内容：
1. 在使用 commit 命令之前，使用 git add 将更改增量地 “添加” 到索引中（注意：即使是修改过的文件也必须 “添加” ）；
2. 在使用 commit 命令之前，再次使用 git rm 从工作树和索引中删除文件；
3. 通过将文件列为 commit 命令的参数，在这种情况下，commit 将忽略索引中的更改，而是记录所列文件的当前内容；
4. 通过将 -a 开关与 commit 命令一起使用，自动 “添加” 所有已知文件（即索引中已列出的所有文件）中的更改，并自动 “rm” 索引中已从工作树中删除的文件，然后执行实际的 commit。

通过给定相同的参数集（选项和路径），可以使用 --dry-run 选项来获得上面任何一项所包括的内容的摘要，以便进行下一次提交。

如果您进行了提交，然后立即发现错误，您可以使用 git reset 从中恢复。

## OPTIONS
$ git commit
		-c <commit>  # 添加提交说明
		-a | --all  # 自动添加所有已知文件（即索引中已列出的所有文件）中的更改，并自动 rm 索引中已从工作树中删除的文件，然后执行实际的 commit 操作。
		--amend  # 修补提交。本质是通过创建新的提交来替换当前分支的顶端。新提交与当前提交具有相同的父级和作者。修补提交最明显的价值是可以稍微改进你最后的提交，而不会让 “啊，忘了添加一个文件” 或者 “小修补，修正笔误” 这种提交信息弄乱你的仓库历史。
		--dry-run  # 不要创建提交，而是显示要提交的路径列表、具有未提交的本地更改的路径以及未跟踪的路径。
		--short  # 进行试运行时，以短格式给出输出。

## EXAMPLES
1）试运行，不要创建提交，并以短格式给出输出。
$ git commit -m 'commit: new file[test.txt], modified[244.md]' --dry-run --short
A   test.txt
M   "...\244.md"

2）将文件列为 commit 命令的参数，在这种情况下，commit 将忽略索引中的更改，而是记录所列文件的当前内容；
$ git commit -m 'commit: new file[test.txt]' test.txt
[main 18f0883] commit: new file[test.txt]
  1 insertion(+)
  create mode 100644 test.txt

3）通过将 -a 开关与 commit 命令一起使用，自动 “添加” 所有已知文件（即索引中已列出的所有文件）中的更改，并自动 “rm” 索引中已从工作树中删除的文件，然后执行实际的 commit。
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
		deleted: test.txt
		modified: "...\244.md"

$ git commit -m 'commit: deleted[test.txt], modified[244.md]' -a
```

# 12、git reset

## 12.1 命令详解

```
$ git reset --help | -h
## NAME
将当前 HEAD（最新提交） 重置为指定的提交 <commit> 状态。

## DESCRIPTION
将当前 HEAD 重置为指定的提交状态，工作树和索引中的任何更改都将被丢弃。

## OPTIONS
$ git reset
		--mixed（默认值）  # 混合重置。只重置索引项，但不重置工作树（即，已更改的文件被保留，但未标记为提交），并报告尚未更新的内容。这是默认操作。
		--soft  # 软重置。根本不接触索引文件或工作树（只将 HEAD 头重置为 <commit>，就像所有模式一样）。这使得所有更改后的文件都是 Changes to committed，正如 git status所说的那样。
		--hard  # 硬重置。重置索引项和工作树。自 <commit> 以来，对工作树中被跟踪文件的任何更改都将被丢弃。任何未跟踪的文件或目录在写入任何跟踪的文件时都会被简单地删除。
		--merge  # 合并重置。重置索引项并更新工作树中 <commit> 和 HEAD 之间不同的文件，但保留索引和工作树之间不同的那些文件（即，具有尚未添加的更改）。如果 <commit> 和索引之间不同的文件有未记录的更改，则重置将中止，即取消与工作树中的内容有冲突的合并。
		--keep  # 保留重置。重置索引项并更新工作树中 <commit> 和 HEAD 之间不同的文件。如果 <commit> 和 HEAD 之间不同的文件具有本地更改，则重置将中止。
```

## 12.2 使用场景及案例

### 12.2.1 混合重置：撤销添加

```
第一步：创建 reset.txt 新文件，修改 hello.txt 文件，删除 test.txt 文件，并添加至索引
$ touch reset.txt
$ echo 'world' >> hello.txt
$ rm test.txt
$ git add .
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
		new file: reset.txt
		modified: hello.txt
		deleted:  test.txt

第二步：使用混合重置（默认值），将索引重置至工作态，并报告尚未更新的内容。
[逐一重置]
$ git reset -- hello.txt
Unstaged changes after reset:
M    hello.txt
$ git reset -- test.txt
Unstaged changes after reset:
D    test.txt
或者
[全部重置]
$ git reset
Unstaged changes after reset:
M    hello.txt
D    test.txt

$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
		modified: hello.txt
		deleted:  test.txt
Untracked files:
  (use "git add <file>..." to include in what will be committed)
		reset.txt
```

### 12.2.2 软重置：撤销提交并重做

```
第一步：创建 reset.txt 新文件，修改 hello.txt 文件，删除 test.txt 文件，并提交至当前分支
$ touch reset.txt
$ echo 'world' >> hello.txt
$ rm test.txt
$ git add .
$ git commit -m 'the latest commit' -a
$ git status
On branch main
Your branch is ahead of 'origin/main'.
nothing to commit, working tree clean.

第二步：使用软重置：只将 HEAD 头重置为 ＜commit＞，完成撤销提交并重做
$ git reset --soft HEAD^
$ git status
On branch main
Your branch is ahead of 'origin/main'.
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
		new file: reset.txt
		modified: hello.txt
		deleted:  test.txt
```

### 12.2.3 硬重置：永久撤销提交（慎用）

场景分析：最后三个提交（HEAD、HEAD^ 和 HEAD~2）都很糟糕，您不想再看到它们了。

```
第一步：创建 reset.txt 新文件，修改 hello.txt 文件，删除 test.txt 文件，并提交至 matser 分支
$ touch reset.txt
$ git add reset.txt
$ git commit -m 'the first commit' reset.txt

$ echo 'world' >> hello.txt
$ git commit -m 'the second commit' hello.txt

$ git rm test.txt
$ git commit -m 'the third commit' test.txt

第二步：重置 master 分支以完全消除这三个提交
$ git reset --hard HEAD~3
```

### 12.2.4 硬重置：创建一个主题分支，然后再永久撤销提交（推荐）

场景分析：
- 1. 您已经进行了一些提交，但意识到它们进入 master 分支还为时过早。您希望在主题分支中继续完善它们，所以在当前 HEAD 的基础上创建 topic/wip 分支。
- 2. 然后重置 master 分支以消除这三个提交。
- 3. 最后，切换到 topic/wip 分支并继续工作。

```
第一步：创建 reset.txt 新文件，修改 hello.txt 文件，删除 test.txt 文件，并提交至 matser 分支
$ touch reset.txt
$ git add reset.txt
$ git commit -m 'the first commit' reset.txt

$ echo 'world' >> hello.txt
$ git commit -m 'the second commit' hello.txt

$ git rm test.txt
$ git commit -m 'the third commit' test.txt

第二步：在当前 HEAD 的基础上创建 topic/wip 分支
$ git branch topic/wip

第三步：重置 master 分支以消除这三个提交
$ git reset --hard HEAD~3

第四步：切换到 topic/wip 分支并继续工作
$ git switch topic/wip
```

# 13、查看对象信息
## 13.1 git log

```
$ git log --help | -h
## NAME
显示提交日志。

## DESCRIPTION
在提交了若干更新，又或者克隆了某个项目之后，你也许想回顾下提交历史。 完成这个任务最简单而又有效的工具是 git log 命令。

## OPTIONS
$ git log
	## 1）限制输出格式，方便查阅。
		-p | --patch  # 显示每次提交所引入的差异（按补丁的格式输出）。
		--stat  # 显示每次提交的简略统计信息，包括在每次提交的下面列出所有被修改过的文件、有多少文件被修改了以及被修改过的文件的哪些行被移除或是添加了。 在每次提交的最后还有一个总结。
		--all  # 将 refs/ 中的所有 refs 以及 HEAD 在命令行上都列为 <commit>。
		--decorate  # 打印出所显示的任何提交的分支名称。默认情况下，不会打印 ref 名称前缀 refs/heads/、refs/tags/ 和 refs/remotes/。如果指定了 full，则将打印完整的引用名称（包括前缀）。
		-g | --walk-reflogs  # 以标准日志的格式输出 git reglog 输出的所有引用日志。
		--graph  # 在日志旁以 ASCII 图形显示分支与合并历史。
		--abbrev-commit  # 显示简短且唯一的提交 SHA-1 值，默认使用七个字符，不过有时为了避免歧义，会增加字符数。
		--oneline  # 使用一行显示历史提交信息。
		--pretty  # 使用其他格式显示历史提交信息。可用的选项包括 oneline、short、full、fuller 和 format（用来定义自己的格式）。
	## 2）限制输出选项，过滤结果。
		oldref...newref | newref --not oldref # 显示所有在后面 oldref 分支但不在前面 newelf 分支的提交的列表。	
		-n  # 仅显示最近的 n 条提交。
		--after   # 仅显示指定时间之后的提交。
		--before  # 仅显示指定时间之前的提交。
		--not  # 仅显示指定提交或引用之后的提交。
		--author  # 仅显示作者匹配指定字符串的提交。
		--no-merges  # 不显示合并提交。
		-S  # 仅显示添加或删除内容匹配指定字符串的提交。 

## EXAMPLES
1）自定义日志记录的输出格式
$ git log --pretty=format:"%h - %au, %ar : %s"  # 记住这条常用命令就行了。
常用选项：
%h    提交的简写哈希值
%s    提交说明
%an   作者名字
%ar   作者对文件的修订日期，按多久以前的方式显示
e7b4a31 - LiChao, 13 hours ago : all
c02cd3a - LiChao, 13 hours ago : new file: hello.txt
... ...

2）自定义日志记录的输出格式，并在日志旁以 ASCII 图形显示分支与合并历史
$ git log --pretty=format:"%h %s" --graph  # 记住这条常用命令就行了。
* 2d3acf9 ignore errors from SIGCHLD on trap
*  5e3ee11 Merge branch 'master' of git://github.com/dustin/grit
|\
| * 420eac9 Added a method for getting the current branch.
* | 30e367c timeout code and tests
* | 5a09431 add timeout protection to grit
* | e1193f8 support for heads with slashes in them
|/
* d6016bc require time for xmlschema
*  11d191e Merge branch 'defunkt' into local

3）自定义日记记录的输出格式和输出选项
$ git log --pretty=format:"%h - %s" --author='Junio C Hamano' --since='2008-10-01' --before='2008-11-01' --no-merges -- t/
5610e3b - Fix testcase failure when extended attributes are in use
acd3b9e - Enhance hold_lock_file_for_{update,append}() API
f563754 - demonstrate breakage of detached checkout with symbolic link HEAD
d1a43f2 - reset --hard/read-tree --reset -u: remove unmerged new paths
51a94af - Fix "checkout --track -b newbranch" on detached HEAD
b0ad11e - pull: allow "git pull origin $something:$current_branch" into an unborn branch
在近 40000 条提交中，上面的输出仅列出了符合条件的 6 条记录。

4）查看当前分支的最后一次提交
$ git log HEAD -1
commit b2ff05a785f78c08e9fed8e6f93ad0e0d7335430 (HEAD -> main, dev)
Author: LiChao <18810396626@163.com>
Date:   Wed Nov 29 09:46:31 2023 +0800
    new file [pretty.log]

5）查看所有提交历史、各个分支的指向以及分叉情况
$ git log --oneline --decorate --graph --all
* c2b9e (HEAD, master) made other changes
| * 87ab2 (testing) made a change
|/
* f30ab add feature #32 - ability to add new formats to the
* 34ac2 fixed bug #1328 - stack overflow under certain conditions
* 98ca9 initial commit of my project

6）比较新抓取的 featureA 分支和 远程 origin/featureA 分支之间的提交的差异列表。
$ git log featureA...origin/fearureA
commit aad881d154acdaeb2b6b18ea0e827ed8a6d671e6
Author: John Smith <jsmith@example.com>
Date:   Fri May 29 19:57:33 2009 -0700

    changed log output to 30 from 25
```

## 13.2 git shortlog（生成提交简报）

```
$ git shortlog --help | -h
## NAME
汇总 git log 输出。

## DESCRIPTION
以适合包含在发布公告中的格式总结 git 日志输出，每个提交都将按作者和标题分组。

使用 git shortlog 命令可以快速生成一份包含从上次发布之后项目新增内容的修改日志（changelog）类文档。

## EXAMPLES
$ git shortlog --no-merges master --not v1.0.1
hris Wanstrath (6):
      Add support for annotated tags to Grit::Tag
      Add packed-refs annotated tag support.
      Add Grit::Commit#to_patch
      Update version and History.txt
      Remove stray `puts`
      Make ls_tree ignore nils

Tom Preston-Werner (4):
      fix dates in history
      dynamic version method
      Version bump to 1.0.2
      Regenerated gemspec for version 1.0.2

这份整洁的总结包括了自 v1.0.1 以来的所有提交，并且已经按照作者分好了组。
```

## 13.3 git reflog

```
$ git reflog --help | -h
## NAME
管理引用日志信息。

## DESCRIPTION
此命令管理记录在回流中的信息。

引用日志或 reflogs 记录了最近几个月你的 HEAD 和分支引用所指向的历史。每当你的 HEAD 所指向的位置发生了变化，Git 就会将这个信息存储到引用日志这个历史记录里。你也可以通过 reflog 数据来获取之前的提交历史。
反射在各种 Git 命令中都很有用，可以指定引用的旧值。
例如：
HEAD@{n} 表示目前所处的位置为 HEAD 的倒数第 n + 1 个提交。
	HEAD@{0} ->  目前所处的位置为 HEAD 的倒数第 1 个提交，即上一个提交，即最新提交。 
	HEAD@{1} ->  目前所处的位置为 HEAD 的倒数第 2 个提交。
	HEAD@{2} ->  目前所处的位置为 HEAD 的倒数第 3 个提交。
	... ...
master@{one.week.ago} 表示一周前 master 分支指向了哪个提交，依此类推。
这个方法只对还在你引用日志里的数据有用，所以不能用来查好几个月之前的提交。例如，git show HEAD@{2.months.ago} 这条命令只有在你克隆了一个项目至少两个月时才会显示匹配的提交，如果你刚刚克隆了仓库，那么它将不会有任何结果返回。

## NOTES
值得注意的是，引用日志只存在于本地仓库，它只是一个记录你在自己的仓库里做过什么的日志。
当你新克隆一个仓库的时候，引用日志是空的，因为你在仓库里还没有操作。
即将引用日志想作 Git 版的 shell 历史记录， 重点在于仅与你和你的会话相关，而与他人无关。

## OPTIONS
$ git reflog 
		show  # 默认子命令，显示命令行中提供的引用的日志（默认情况下为 HEAD ）。reflog 涵盖了最近的所有操作，此外 HEAD reflog 还记录了分支切换。git reflog show是 git log -g 的别名，以标准日志的格式输出 git reglog 输出的所有引用日志。
		expire  # 从 reflog 中删除早于过期时间的 ref 条目。它可以帮助清理不再需要的旧引用日志，以节省存储空间。
			--all  # 删除所有过期（默认为 7 天）的引用日志。
			--expire=<time>  # 设置过期时间，单位为天。例如，--expire=7 表示删除一周前的引用日志。
			--prune  # 同时删除过期的引用日志和对应的提交对象。

1）查看引用日志
$ git reflog [show] -n 5
3576de1 (HEAD -> main, origin/main, origin/HEAD) HEAD@{0}: checkout: moving from dev to main  # 3576de1 为当前 main 分支的最新提交，在这个提交之后，发生了分支切换，有一个名为 dev 的分支被切换到了 main 分支，刚好 HEAD 从 dev 分支指向了 main 分支，最新提交为 3576de1，和开头保持一致。
b2ff05a (dev) HEAD@{1}: checkout: moving from main to dev  # b2ff05a 为 dev 分支的提交，目前所处的位置为 HEAD 的倒数第 2 个提交，在这个提交之后，发生了分支切换，有一个名为 main 的分支被切换到了 dev 分支，刚好 HEAD 从 main 分支指向了 dev 分支，所以当前分支为 dev 分支，最新提交为 b2ff05a，和开头保持一致。
3576de1 (HEAD -> main, origin/main, origin/HEAD) HEAD@{2}: commit: add new content  # 3576de1 为当前 main 分支的倒数第 3 个提交，提交说明内容为 add new content 。
b2ff05a (dev) HEAD@{3}: checkout: moving from test to main  # b2ff05a 为 dev 分支的提交，目前所处的位置为 HEAD 的最近第 4 个提交，在这个提交之后，发生了分支切换，有一个名为 test 的分支被切换到了 main 分支。
b2ff05a (dev) HEAD@{4}: checkout: moving from main to test  # b2ff05a 为 dev 分支的提交，目前所处的位置为 HEAD 的最近第 5 个提交，在这个提交之后，发生了分支切换，有一个名为 main 的分支被切换到了 test 分支。

2）删除一周前的引用日志
$ git reflog expire --expire=7
```

## 13.4 git show

```
$ git show --help | -h
## NAME
显示各种类型的对象。

## DESCRIPTION
显示一个或多个对象（blobs、trees、tags 和 commits ）。
1. 对于提交对象，它显示日志消息和文本差异。它还以 git-diff-tree --cc 生成的特殊格式显示合并提交。
2. 对于标签对象，它显示标签消息和引用的对象。
3. 对于树对象，它显示名称（相当于 git-ls-tree with--name only）。
4. 对于数据对象，它显示普通内容。

## OPTIONS
$ git show <object>
		-s | --summary  # 仅显示摘要信息
		--stat  # 显示提交的简略统计信息，包括在每次提交的下面列出所有被修改过的文件、有多少文件被修改了以及被修改过的文件的哪些行被移除或是添加了。 在每次提交的最后还有一个总结。
		-c | --cc  # 显示所有包含在提交对象中的合并提交

## EXAMPLES
1）显示指定提交对象的详细信息或摘要信息或简略统计信息或合并提交信息
$ git show b2ff

2）显示指定提交对象的摘要信息或简略统计信息或合并提交信息
$ git show b2ff -s | --stat | -c
```