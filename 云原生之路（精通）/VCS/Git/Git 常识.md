
# 1、文件模式

Git 文件模式参考了常见的 UNIX 文件模式，但远没那么灵活，包括**普通文件、可执行文件、符号连接**等数据对象的文件模式，以及**数据对象**包、**树对象**包、**提交对象**包、**标签对象**包等对象包的文件模式，还有用于目录、子模块的文件模式等。

- `040000`：目录、**树对象**
- `100644`：普通文件
- `100755`：执行文件、可执行文件
- `120000`：符号链接、压缩文件、归档文件等
- `130000`：**数据对象（普通文件、可执行文件、符号链接）**包（Data Object pack）
- `140000`：索引包（Index pack）
- `150000`：**树对象**包（Tree object pack）
- `160000`：打包文件、**提交对象**包（Commit object pack）
- `170000`：**标签对象**包（Tag object pack）、子模块。

# 2、引用规范（refspec）

`<refspec>`  是一个用于规范本地与远程分支映射关系的字符串。它通常用于 `git fetch`、`git pull`、`git push` 和 `git rebase` 等命令中，用于指定要操作的分支和它们之间的关系。

`<refspec>` 的组成元素包括 `<src>`、`<dst>`、`+`、`:`、` `、`...`、`*` 等。

- `<src>`：源分支，可以是本地分支，也可以是远程版本库中的分支，具体语义根据当前所处的命令进行区分。例如，如果是在 `git fetch | pull` 命令中，`<src>` 则指的是远程分支；如果是在 `git pull` 命令中，`<src>` 则指的是本地分支。
- `<dst>`：目标分支，同上取反。例如，如果是在 `git fetch | pull` 命令中，`<dst>` 则指的是本地 `.git/refs/` 中保存的远程分支信息；如果是在 `git push` 命令中，`<dst>` 则指的是远程分支。
- `:`：分隔符，结合以上元素，存在以下几种场景：
		- `git fetch | pull <remote> <src>:<dst>`：将远程分支 `<src>` 拉取到本地 `.git/refs` 中保存的远程分支信息内。
		- `git push <remote> <src>:<dst>`：将本地分支 `src` 推送到远程版本库的分支内。
		- `git push <remote> :<dst>`：删除远程分支，即故意把 `<src>` 留空，意味着把远程分支 `<dst>` 定义为空值，也就是删除远程分支的意思。
- `+`：告诉 Git 即使在不能快进（not fast forward）的情况下也要（强制，先合并）更新分支。
	- `not fast forward`：通常情况下，远程分支的最新提交永远落后于或一致于本地分支。但是，在多人协同开发的情况下，难免会出现远程分支的最新提交超出于本地分支的情况，这些提交在本地分支中是不存在的。而在此时执行 `git fetch | push` 时会发生失败，出现 `not fast forward` 不能快进的问题提示。解决这个问题的办法通常是使用 `git pull | merge | rebase` 命令来讲远程分支的最新提交合并到本地分支中。这样，当再次 `fetch` 或 `push` 时，Git 就能够快进操作了。而 `+` 做的就是后面这个事情。
- ` ` ：空格，表示 `merge` 合并操作，例如，`git fetch <remote> <src> <dst>`，将远程分支 `<src>` 合并到本地 `<dst>`；`git push <remote> <srt> <dst>` 将本地分支合并到远程分支。
- `...`：省略号，表示同时操作两个分支，例如，`devA...devB`，表示同时操作 `devA` 和 `devB` 分支。
- `*`：星号，表示同时操作零到多个分支，例如，`refs/heads/*`，表示所有分支。

Git 中引用路径规范如下：

```
|- .git/
    ... ...
    |- HEAD  [ref: refs/heads/dev]  # HEAD 符号引用，内容为一个典型的引用路径，指向当前分支。
    |- refs/  # 引用信息
        |- heads/  # 所有本地分支信息
            |- master  [master_commit_sha1]  # master 分支的最新提交对象的 sha1 值
            |- dev  # 同上
            |- test  # 同上
        |- remotes/  # 所有本地保留的远程分支信息
            |- origin/  # 本地设置的远程仓库名称
                |- master  [remote_master_commit_sha1]  # 本地保留的远程仓库 master 分支的最新提交对象的 sha1 值
                |- dev  # 同上
                |- test  # 同上
                |- HEAD  [ref: refs/remotes/origin/dev]  # 本地保留的远程仓库 HEAD 符号引用，指向当前分支。
        |- tags/  # 所有标签信息
    ... ...
```

通常情况下，`refs/heads/`、`refs/remotes/`、`refs/tags/` 作为标准前缀，可以省略。例如，
- `refs/heads/dev` = `dev`，同样表示本地或远程仓库的 `dev` 分支。
- `refs/remotes/origin/dev`  = `origin/dev`，同样表示本地保留的远程仓库 `origin` 的 `dev` 分支。

# 3、冲突

## 3.1 冲突是如何产生的？

在合并过程中，工作树文件会更新以反映合并的结果。在对共同祖先的版本所做的更改中，不重叠的更改（即，您更改了文件的某个区域，而另一方保持该区域不变，反之亦然）会逐字逐句地包含在最终结果中。然而，当双方都对同一区域进行更改时，Git 不能随机选择一方而不是另一方，并要求你通过留下双方对该区域所做的事情来解决问题。

默认情况下，Git 使用与 RCS 套件中的 “merge” 程序相同的样式来呈现这样一个冲突的大块，如下所示：

```
这里的行要么与共同的祖先保持不变，要么因为只有一侧发生了变化而被干净地解析，要么因为两侧都以相同的方式发生了变化。
<<<<<<< 你更改的内容:sample.txt
冲突解决很难；
我们去购物吧。
=======
Git 使冲突解决变得容易。
>>>>>>> 他们更改的内容：sample.txt
这是另一行，它被干净地解析或未修改。
```

发生一对更改冲突的区域用标记 `<<<<<<<`、`=======` 和 `>>>>>>>` 标记。`=====` 之前的部分通常是你（**当前分支的修改**）的一侧，之后的部分通常为他们（**目标分支的修改**，被合并的分支）的一侧。

通过将 `merge.conflictStyle` 配置变量设置为 `diff3` 或 `zdiff3` ，可以使用其他样式。

在 `diff3` 样式中，上述冲突可能如下所示：

```
这里的线要么与共同祖先保持不变，要么因为只有一侧发生了变化而被干净地解析，
<<<<<<< 你更改的内容:sample.txt
或者因为双方都以相同的方式更改而被干净地解决。
解决冲突很难；
我们去购物吧。
||||||| 原文内容:sample.txt
或干净地解析，因为双方更改相同。
解决冲突很难。
=======
或者因为双方都以相同的方式改变而彻底解决。
Git 使冲突解决变得容易。
>>>>>>> 他们更改的内容：sample.txt
这是另一行，它被干净地解析或未修改。
```

而在 `zdiff3` 风格中，它可能看起来是这样的：

```
这里的行要么与共同的祖先保持不变，要么因为只有一侧发生了变化而被干净地解析，要么因为两侧都以相同的方式发生了变化。
<<<<<<< 你更改的内容:sample.txt
冲突解决很难；
我们去购物吧。
||||||| 原文内容:sample.txt
或干净地解析，因为双方更改相同。
解决冲突很难。
=======
Git使冲突解决变得容易。
>>>>>>> 他们更改的内容：sample.txt
这是另一行，它被干净地解析或未修改。
```

除了使用 `<<<<<<<`、`=======` 和 `>>>>>>>` 标记外，它还使用了另一个后面跟有 `|||||||` 来显示原文内容的标记。你可以看出，原文（**修改前的内容**）只是陈述了一个事实，而你的一方只是屈服于这个陈述并放弃了，而另一方则试图采取更积极的态度。有时你可以通过查看原件来获得更好的分辨率。

## 3.2 解决冲突

使用 `git status` 命令来查看那些因包含合并冲突而处于未合并（unmerged）状态的文件：

```
$ git status
On branch master
You have unmerged paths.
  (fix conflicts and run "git commit")

Unmerged paths:
  (use "git add <file>..." to mark resolution)

    both modified:      index.html

no changes added to commit (use "git add" and/or "git commit -a")
```

为了解决冲突，你必须选择使用由 `=======` 分割的两部分中的一个，或者你也可以自行合并这些内容。

在你解决了所有文件里的冲突之后，对每个文件使用 `git add` 命令来将其标记为冲突已解决。 一旦暂存这些原本有冲突的文件，Git 就会将它们标记为冲突已解决。

如果你想使用图形化工具来解决冲突，你可以运行 `git mergetool`，该命令会为你启动一个合适的可视化合并工具，并带领你一步一步解决这些冲突。

```
$ git mergetool

This message is displayed because 'merge.tool' is not configured.
See 'git mergetool --tool-help' or 'git help config' for more details.
'git mergetool' will now attempt to use one of the following tools:
opendiff kdiff3 tkdiff xxdiff meld tortoisemerge gvimdiff diffuse diffmerge ecmerge p4merge araxis bc3 codecompare vimdiff emerge
Merging:
index.html

Normal merge conflict for 'index.html':
  {local}: modified file
  {remote}: modified file
Hit return to start merge resolution tool (opendiff):
```

等你退出合并工具之后，Git 会询问刚才的合并是否成功。 如果你回答是，Git 会暂存那些文件以表明冲突已解决： 你可以再次运行 `git status` 来确认所有的合并冲突都已被解决：

```
$ git status
On branch master
All conflicts fixed but you are still merging.
  (use "git commit" to conclude merge)

Changes to be committed:

    modified:   index.html
```

如果你对结果感到满意，并且确定之前有冲突的的文件都已经暂存了，这时你可以输入 `git commit` 来完成合并提交。 默认情况下提交信息看起来像下面这个样子：

```
Merge branch 'iss53'

Conflicts:
    index.html
#
# It looks like you may be committing a merge.
# If this is not correct, please remove the file
#	.git/MERGE_HEAD
# and try again.


# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# On branch master
# All conflicts fixed but you are still merging.
#
# Changes to be committed:
#	modified:   index.html
#
```

如果你觉得上述的信息不够充分，不能完全体现分支合并的过程，你可以修改上述信息， 添加一些细节给未来检视这个合并的读者一些帮助，告诉他们你是如何解决合并冲突的，以及理由是什么。

# 4、常用配置选项

```
## 常用配置变量及其值：
$ vim /etc/gitconfig | $HOME/.gitconfig | $REPO/.git/config
[core]
	## 配置 Git 使用的文本编辑器
	editor = "'C:\Program Files (x86)\Notepad++\notepad++.exe' -multiInst -notabbar -nosession -noPlugin"
[alias]
	## 配置 Git 命令中使用的别名
	co = checkout
[user]
	## 设置用户名和邮件地址，这一点很重要，因为每一个 Git 提交都会使用这些信息，它们会写入到你的每一次提交中，不可更改。
	name = LiChao
	email = 18810396626@163.com
[sendemail]
	## 设置 SMTP 邮箱信息
	smtpencryptgion = tls
	smtpserver = smtp.gmail.com
	smtpuser = user@gmail.com
	smtpserverport = 587
[credential]
    ## 如果你正在使用 HTTPS URL 来推送，Git 服务器会询问用户名与密码。 如果不想在每一次推送时都输入用户名与密码，使用缓存机制将其保存在内存中几分钟。
	helper = cache
[remote]
	## 将默认推送远程仓库设置为 origin
	pushDefault = origin
[remote "origin"]  # 定义了一个名为 origin 的（默认）远程仓库
	url = https://gitlab.com/li2chao/docs.git  # 远程仓库 URL
	fetch = +refs/heads/*:refs/remotes/origin/*  # 如何抓取远程仓库的分支内容，此命令告诉 Git 当我们执行 git fetch 时，从远程仓库获取所有以 refs/heads/* 开头的引用（即所有分支），并将它们映射到本地的 refs/remotes/origin/* 的相应位置。
	fetch = +refs/pull/*/head:refs/remotes/origin/pr/*  # 如何抓取远程仓库的合并请求（pr），此命令告诉 Git 当我们执行 git fetch 时，远程仓库中所有的合并请求（pr，假分支）在本地像分支一样展现，即映射到本地的 refs/{pr#}/head 分支。
[branch "main"]  # 定义了一个名为 mian 的本地分支。
	## 配置本地 main 分支信息
	remote = origin  # 设置本地 main 分支的远程仓库为 origin，这意味着本地 main 分支的所有更改都将推送到 origin 远程仓库。
	merge = refs/heads/main  # 设置本地 main 分支的合并引用，这意味着当本地 main 分支需要合并时，Git 将使用 refs/heads/main（远程仓库中 mian 分支的最新提交）作为合并的基础。
[rerere]
	## 启用 rerere（reuse recorded resolution，重用已记录的冲突解决方案）。之后，Git 将会维护一些成功合并之前和之后的镜像，当 Git 发现之前已经修复过类似的冲突时，会使用之前的修复方案，而不需要你的干预。
	enabled = true
```

# 5、.git 存储库目录结构

```
# tree .git/
|-- .git/  # 存放存储库中的所有数据及信息。如果设置了 $GIT_DIR 环境变量，则它会指定要使用的路径，而不是 ./.git 。
	|-- config  # 配置文件，存储库特有的配置选项。
	|-- HEAD  [ref: refs/heads/main]  # 符号引用，当前分支的别名，内容为指向当前分支的引用路径。
	|-- index  # 索引文件，存放暂存区的信息。
	|-- objects/  # 对象数据库，存放存储库中的所有已经提交的数据。如果对象存储目录是通过 $GIT_OBJECT_DIRECTORY 环境变量指定的，则会在下面创建 sha1 目录，否则将使用默认的 $GIT_DIR/objects 目录。
		|-- sha-1(0,2)/
			|-- sha-1(2,40)
		|-- ... ...
		|-- info/
			|-- http-alternates
		|-- pack/
			|-- pack-... .pack  # 包文件，运行 git gc --auto 命令执行自动垃圾回收，收集所有松散对象（不在包文件中的对象）并将它们放置到包文件中，将多个包文件合并为一个大的包文件，移除与任何提交都不相关的陈旧对象。
			|-- pack-... .idx  # 包索引文件，记录了这个包文件所包含的所有对象的 SHA-1 值，和该对象存在于包文件中的偏移量。
	|-- info/  # 忽略信息，包含一个全局性排除（global exclude）文件， 用以放置那些不希望被记录在 .gitignore 文件中的忽略模式（ignored patterns）。
		|-- exclude  # 存放被忽略文件的 glob 规则信息。
	|-- refs/  #  引用信息，存放所有的引用信息。所谓引用，本质上就是数据对象、树对象、提交对象的 SHA-1 值的别名。
		|-- heads/  # 本地分支信息。所谓分支，本质上就是一系列提交之首的 SHA-1 值的别名。
			|-- main  [5448049ea9af0d2aa33667444fd9c67b9e67cde6]  # 本地 main 分支
		|-- pull/  # 合并请求（pr）分支（只读）信息，所谓合并请求（pr），指的是远程仓库中所有已打开的合并请求，它们实际上是分支，但因为它们不在 refs/heads/ 中，所以正常情况下你克隆时不会从服务器上得到它们，抓取过程正常情况下会忽略它们。除非你显式的进行抓取 git fetch origin refs/pull/1/head 或 git fetch origin pr/1，或者在 git config 中配置了 <refspath> 抓取路径映射 fetch = +refs/pull/*/head:refs/remotes/origin/pr/*，然后执行 git fetch。
			|-- 1/head  # 指向远程仓库打开的合并请求分支（pr/1）。
			|-- 1/merge  # 指向合并后的提交记录。
			... ...
		|-- remotes/  # 远程跟踪分支（只读）信息，所谓远程跟踪分支，指的是远程分支状态的引用，它们是你无法移动的本地引用。一旦你进行了网络通信， Git 就会为你移动它们以精确反映远程仓库的状态。请将它们看做书签， 这样可以提醒你该分支在远程仓库中的位置就是你最后一次连接到它们的位置。
			|-- origin/  # 远程仓库的名称
				|-- main  [5448049ea9af0d2aa33667444fd9c67b9e67cde6]  # 远程仓库 main 分支
				|-- HEAD  [ref: refs/remotes/origin/main]  # HEAD 符号引用，内容为指向远程仓库当前分支的引用路径
		|-- tags/  # 所有标签信息。所谓标签，本质上就是一个引用，分为轻量标签和附注标签。所谓轻量标签，本质上就是一个固定的引用；所谓附注标签，是一个指向标签对象的引用路径，标签对象的实现可以参考提交对象，提交对象本质是记录顶层树对象和父提交对象的 SHA-1 值的一种特殊的对象，另外包含了 who（提交者）、when（提交实践）、what（提交说明） 等信息；而标签对象本质是记录 Git 中各种对象的 SHA-1 值的一种特殊的对象，另外包含了 who（提交者）、when（提交实践）、what（提交说明） 等信息。
	|-- hooks/  # 钩子信息，包含客户端或服务端的钩子脚本（hook scripts）。
	|-- logs/  # 日志信息，存放所有的日志信息。
	|-- ... ...
```

# 6、Git 常用环境变量

Git 总是在一个 `bash` shell 中运行，并借助一些 shell 环境变量来决定它的运行方式。有时候，知道它们是什么以及它们如何让 Git 按照你想要的方式去运行会很有用。 这里不会列出所有的 Git 环境变量，但我们会涉及最有用的那部分。

- `GIT_EXEC_PATH`：决定 Git 到哪找它的子程序 （像 `git-commit`, `git-diff` 等等）。 你可以用 `git --exec-path` 来查看当前设置。
- `GIT_DIR`：是 `.git` 目录的位置。 如果这个没有设置， Git 会按照目录树逐层向上查找 `.git` 目录，直到到达 `~` 或 `/`。
- `GIT_WORK_TREE`：是非空版本库的工作目录的根路径。 如果指定了 `--git-dir` 或 `GIT_DIR` 但未指定 `--work-tree`、`GIT_WORK_TREE` 或 `core.worktree`，那么**当前工作目录就会视作工作树的顶级目录**。
- `GIT_OBJECT_DIRECTORY`：指定 `.git/objects` 目录的位置。
- `GIT_NAMESPACE`：控制有命令空间的引用的访问，与 `--namespace` 标志是相同的。 这主要在服务器端有用， 如果你想在一个版本库中存储单个版本库的多个 fork, 只要保持引用是隔离的就可以。

# 7、fast-forward、fast-forward-only、no-fast-forward、non-fast-forward 的区别

- `fast-forward`、`fast-forward-only`、`no-fast-forward` 为 `merge` 时的三个不同的分支合并选项，代表着三种不同的**合并方式**。
	- `fast-forward`：快进模式，默认选项。`git merge <branch> --ff`，尽可能将合并解析为快进（**只更新源分支的指针指向被合并分支的最新提交，不要创建合并提交**）。如果不可能（当合并的历史记录不是当前历史记录的后代时），再创建合并提交（创建一个新的提交对象，同时指向源分支前一个提交对象以及被合并分支的最新提交对象）。
	- `fast-forward-only`：仅快进模式。`git merge <branch> --ff-only`，尽可能将合并作为快进解决（**只更新源分支的指针指向被合并分支的最新提交，不要创建合并提交**），如果不可能，请拒绝合并并以非零状态退出。
	- `no-fast-forward`：非快进模式。`git merge <branch> --no-ff -m <msg>`，在所有情况下都创建合并提交（**创建一个新的提交对象，同时指向源分支前一个提交对象以及被合并分支的最新提交对象**），即使合并可以作为快进解决。
- `non-fast-forward`：是 `pull` 或 `push` 时的一种错误状态（本地仓库与远程仓库的状态不一致），本质是一种**保护机制**，即**当远程仓库有新的提交，而你的本地仓库也想提交一些改动时，Git 会尝试阻止你直接进行 `pull` 或 `push`，以免丢失本地或远程仓库的数据**。
	- 如果是 `pull` 时遇到了 `non-fast-forward`，Git 的本意是发出错误状态，启动保护机制，保护本地仓库的修改不被覆盖，此时的解决办法有两种：
		- 1）`git fetch origin & git merge origin/<remote-branch-name>`，获取最新的远程分支信息，然后将远程分支合并到本地分支上。如果出现冲突，Git 会提示您进行冲突解决。
		- 2）使用 `-f` 选项进行强制拉取，覆盖掉本地分支的所有修改。
	- 如果是 `push` 时遇到了 `non-fast-forward`，Git的本意是发出错误状态，启动保护机制，保护远程仓库的更新不被覆盖，此时的解决办法有两种：
		- 1）`git pull origin`，先拉取远程仓库的更新到本地，进行合并或变基，然后再推送本地更新到远程仓库。
		- 2）使用 `-f` 选项进行强制推送，覆盖掉远程分支的所有修改。

# 8、README 文件

很多团队会在 `README` 文件里放版本库或项目新人需要了解的所有相关的信息。 它一般包含这些内容：

- 该项目的作用
- 如何配置与安装
- 有关如何使用和运行的例子
- 项目的许可证
- 如何向项目贡献力量

如果在你的版本库中找到 `README` 文件，会把它在项目的首页渲染出来，你可以在文件里植入图片或链接让它更容易理解。

# 9、Git 中的特殊符号

- `O^`：**脱字符，获取（合并提交）祖先**。`HEAD^` 指的是当前分支的第一个父提交；`HEAD^2` 指的是当前分支的第二个父提交，这个语法只适用于合并的提交，因为合并提交会有多个父提交，合并提交的第一父提交是你合并时所在分支（通常为 `master`），而第二父提交是你所合并的分支（例如 `topic`）。
- `O~`：**波浪号，获取（历史提交）祖先**。`HEAD~` 指的是当前分支的第一个父提交，等效于 `HEAD^`；`HEAD~n` 指的是当前分支的父提交的父提交的...父提交。
- `O~n^2`：前两者的组合符号，获取之前引用的第二个父提交（假设它是一个合并提交）。
- `^O | --not O`：**取反，选出不在 `O` 分支中的提交**。
- `..`：**双点，选出在后一个分支中而不在前一个分支中的提交**。`A..B == ^A B == B --not A` 指的是在 `B` 分支中而不在 `A` 分支中的提交。如果其中一边为空，即 `A..` 或 `..B`，Git 会使用 `HEAD` 来代替留空的一边。
- `...`：**三点，选出两个分支的异或提交，即被两个引用之一包含但又不被两者同时包含的提交**。`A...B` 指的是要么在 `A` 分支中，要么在 `B`分支中，但不能同时在 `A` 和 `B` 中的提交，即 `A` 和 `B` 分支的异或提交。这种情形下，通常配合 `--left-right` 选项一块使用，它会显示每个提交到底处于哪一侧（`<`、`>`）的分支，这会让输出数据更加清晰。